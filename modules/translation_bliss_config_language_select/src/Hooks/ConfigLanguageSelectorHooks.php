<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config_language_select\Hooks;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\Schema\Mapping;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\ConfirmFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * @internal
 */
final class ConfigLanguageSelectorHooks implements ContainerInjectionInterface {

  use AutowireTrait;
  use StringTranslationTrait;

  private const ExcludedFormIds = [
    // Langcode is used for custom language here.
    'language_admin_add_form',
    'view_preview_form',
  ];

  const FormIdNeedsLanguageWarning = [
    'block_admin_display_form',
    'flag_list',
    'group_admin_permissions',
    'group_admin_roles',
    /** Yes, @see \Drupal\page_manager_ui\Form\PageVariantConfigureForm::getFormId */
    'page_manage_variant_configure_form',
    'page_manager_access_form',
    'page_manager_add_variant_form',
    'page_manager_general_form',
    'page_manager_layout_builder_form',
    'page_manager_parameters_form',
    'page_manager_parameter_edit_form',
    'page_manager_reorder_variants_form',
    'page_manager_static_context_delete_form',
    'page_manager_variant_add_block_form',
    'page_manager_variant_context_form',
    'page_manager_variant_delete_block_form',
    'page_manager_variant_delete_form',
    'page_manager_variant_edit_block_form',
    'role_settings',
    'taxonomy_overview_vocabularies',
    'user_admin_permissions',
    'user_admin_roles_form',
    'views_ui_admin_settings_basic',
    'views_ui_admin_settings_advanced',
  ];

  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly LanguageManagerInterface $languageManager,
    private readonly TypedConfigManagerInterface $typedConfigManager,
    private readonly MessengerInterface $messenger,
  ) {}

  /**
   * @see \translation_bliss_config_language_select_form_alter()
   */
  function hookFormAlter(array &$form, FormStateInterface $form_state, string $form_id): void {
    if (in_array($form_id, self::ExcludedFormIds)) {
      return;
    }

    $formObject = $form_state->getFormObject();
    if ($formObject instanceof ConfirmFormInterface) {
      return;
    }

    if ($formObject instanceof ConfigFormBase) {
      $configLangcodes = $this->addSelectorToConfigForm($form, $formObject);
    }
    elseif ($formObject instanceof EntityFormInterface) {
      $entity = $formObject->getEntity();
      if ($entity instanceof ConfigEntityInterface) {
        $entity = $this->loadOverrideFree($entity);
        $form = $this->addSelectorToConfigEntityForm($form, $entity);
        $configLangcodes = [$entity->language()->getId()];
      }
    }

    if (in_array($form_id, self::FormIdNeedsLanguageWarning)) {
      $this->addLanguageWarning($configLangcodes ?? NULL);
    }
  }

  public function addLanguageWarning(?array $configLangcodes): void {
    $currentLanguage = $this->languageManager->getCurrentLanguage();
    if ($configLangcodes) {
      $differentConfigLangcodes = array_diff($configLangcodes, [$currentLanguage->getId()]);
      foreach ($differentConfigLangcodes as $configLangcode) {
        $configLanguage = $this->languageManager->getLanguage($configLangcode);
        /** @noinspection HtmlUnknownTarget */
        $this->messenger->addWarning($this->t(
          string: 'Editing %configLanguage config in %currentLanguage will change the config language and its strings. <a href=":switchLink">Switch to %configLanguage</a>.',
          args: [
            '%configLanguage' => $this->languageManager->getLanguage($configLangcode)->getName(),
            '%currentLanguage' => $currentLanguage->getName(),
            ':switchLink' => Url::fromRoute('<current>')->setOption('language', $configLanguage)->toString(),
          ],
        ));
      }
    }
    // No information on config language is available. Add warning for editing
    // in a non-english language.
    elseif (
      $currentLanguage->getId() !== 'en'
      && ($english = $this->languageManager->getLanguage('en'))
    ) {
      /** @noinspection HtmlUnknownTarget */
      $this->messenger->addWarning($this->t(
        string: 'You are editing this config in %currentLanguage. Maybe you want to <a href=":switchLink">switch to %configLanguage</a>.',
        args: [
          '%configLanguage' => $english->getName(),
          '%currentLanguage' => $currentLanguage->getName(),
          ':switchLink' => Url::fromRoute('<current>')
            ->setOption('language', $english)
            ->toString(),
        ],
      ));
    }
  }

  /**
   * Add the config language selector to config forms.
   */
  function addSelectorToConfigForm(array &$form, ConfigFormBase $formObject): array {
    $langcodes = [];
    if (!isset($form['langcode'])) {
      // Get the element / config map.
      $map = [];
      $this->collectConfigTargetMap($form, $map);

      $configNames = array_keys($map);
      $multiConfig = count($configNames) > 1;
      foreach ($configNames as $configName) {
        $typedConfig = $this->typedConfigManager->get($configName);
        $configLabel = $typedConfig->getDataDefinition()->getLabel();

        // From core.data_types.schema.yml:
        // The `langcode` key:
        //  - MUST be specified when there are translatable values
        //  - MUST NOT be specified when there are no translatable values.
        if (
          $typedConfig instanceof Mapping
          && $typedConfig->hasTranslatableElements()
        ) {
          $langcodes[] = $typedConfig->get('langcode')->getString();

          // The value is picked up by ConfigFormBase.
          /** \Drupal\Core\Form\ConfigFormBase::copyFormValuesToConfig */
          $key = sprintf("langcode__%s", str_replace('.', '__', $configName));
          $form[$key] = [
            '#config_target' => "{$configName}:langcode",
            '#title' => $multiConfig ?
              t('Configuration language for %config', ['%config' => $configLabel]) :
              t('Configuration language'),
            '#description' => t('Changes the configuration language. Does not translate any strings.'),
            '#type' => 'language_select',
            // @todo Add configurable whitelist.
            '#languages' => LanguageInterface::STATE_CONFIGURABLE,
            '#weight' => -100,
            // No need for #default_value:
            /** @see \Drupal\Core\Form\ConfigFormBase::loadDefaultValuesFromConfig */
          ];
        }
      }
    }
    return $langcodes;
  }

  /**
   * A lightweight version of config target extraction.
   *
   * No element parents, no duplicate detection.
   *
   * @see \Drupal\Core\Form\ConfigFormBase::doStoreConfigMap
   */
  private function collectConfigTargetMap(array $element, array &$map): void {
    if ($target = $element['#config_target'] ?? NULL) {
      if (is_string($target)) {
        $target = ConfigTarget::fromString($target);
      }
      foreach ($target->propertyPaths as $propertyPath) {
        $map[$target->configName][$propertyPath] = TRUE;
      }
    }
    foreach (Element::children($element) as $key) {
      $this->collectConfigTargetMap($element[$key], $map);
    }

  }

  /**
   * Add the config language selector to config entity forms.
   */
  public function addSelectorToConfigEntityForm(array $form, ConfigEntityInterface $entity): array {
    if (!isset($form['langcode'])) {
      $languages = $this->languageManager->getLanguages();
      $defaultLanguage = $this->languageManager->getDefaultLanguage();
      // Favor 'en' over defaultLanguage for config creation.
      // @todo Make configurable.
      $defaultLangcode = isset($languages['en']) ? 'en' : $defaultLanguage->getId();

      // The value is - if not overridden - picked up by EntityForm.
      /** @see \Drupal\Core\Entity\EntityForm::copyFormValuesToEntity */
      // More about language selector:
      /** @see \Drupal\Core\Render\Element\LanguageSelect */
      /** @see \language_element_info_alter() */
      /** @see \language_process_language_select() */
      $form['langcode'] = [
        '#title' => t('Configuration language'),
        '#description' => t('Changes the configuration language. Does not translate any strings.'),
        '#type' => 'language_select',
        // @todo Add configurable whitelist.
        '#languages' => LanguageInterface::STATE_CONFIGURABLE,
        '#default_value' => $entity->get('langcode') ?? $defaultLangcode,
        '#weight' => -100,
      ];
    }
    return $form;
  }

  /**
   * Get an override free version of the config entity.
   *
   * While config has immutable / editable, config entities do not.
   * Entities from the route controller are loaded override-free on admin theme
   * pages, but this is a hack and there are cases where config entities are
   * loaded in another way. Example: EntityViewDisplay.
   *
   * @link https://drunkenmonkey.at/blog/config_entity_overrides
   */
  function loadOverrideFree(ConfigEntityInterface $entity): ConfigEntityInterface {
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    // This should always be the case.
    if ($storage instanceof ConfigEntityStorageInterface) {
      if ($entity->id()) {
        $entity = $storage->loadOverrideFree($entity->id())
          ?? $entity;
      }
    }
    return $entity;
  }

}

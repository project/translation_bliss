<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\ConfigOverride;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\AutowireServiceClosure;

/**
 * Override config with translations, instead of syncing config override.
 *
 * @internal
 */
final class TranslationBlissConfigOverride implements ConfigFactoryOverrideInterface {

  protected bool $loadingOverrides = FALSE;

  /**
   * @param \Closure(): \Drupal\Core\Language\LanguageManagerInterface $getLanguageManager
   * @param \Closure(): \Drupal\translation_bliss_config\ConfigOverride\TranslationBlissConfigTranslator $getConfigTranslator
   */
  public function __construct(
    #[AutowireServiceClosure('language_manager')]
    private readonly \Closure $getLanguageManager,
    #[AutowireServiceClosure(TranslationBlissConfigTranslator::class)]
    private readonly \Closure $getConfigTranslator,
  ) {}

  public function getLanguageManager(): LanguageManagerInterface {
    return ($this->getLanguageManager)();
  }

  public function getConfigTranslator(): TranslationBlissConfigTranslator {
    return ($this->getConfigTranslator)();
  }

  /**
   * Get configuration target language.
   *
   * This should in fact use LanguageManagerInterface::getConfigOverrideLanguage,
   * but this leads to nasty and hard debug issues. How to reproduce:
   * - Set 'de' as negotiation default.
   * - (Maybe it's relevant to enable search_api and webform_views)
   * Then on `drush cr`, english labels are cached for 'de'.
   * This is because EntityTypeManager caches field definitions for
   * currentLanguage, which is "de", but configOverrideLanguage is "en",
   * because it defaults to defaultLanguage and seems to not have been set yet.
   *
   * Maybe the reason is that sometimes the listener calls currentLanguage
   * before it's initialized, in which case it falls back to 'system', but
   * later when it is initialized, configOverrideLanguage is not updated.
   * If that is the case, finishing initialization of LanguageManager should
   * trigger syncing configOverrideLanguage somehow.
   *
   * And in the end, do we need configOverrideLanguage as a separate concept
   * from currentLanguage (i.e. interface_language) at all?
   *
   * @see \Drupal\Core\Language\LanguageManagerInterface::getConfigOverrideLanguage
   * @see \Drupal\language\EventSubscriber\LanguageRequestSubscriber::setLanguageOverrides
   * @see \Drupal\language\ConfigurableLanguageManager::getCurrentLanguage
   *
   * @todo Use configOverrideLanguage once fixed upstream.
   */
  protected function getTargetLanguage(): LanguageInterface {
    return $this->getLanguageManager()->getCurrentLanguage();
  }

  /**
   * Load overrides.
   *
   * @see \Drupal\locale\LocaleConfigManager::getTranslatableDefaultConfig
   */
  public function loadOverrides($names): array {
    if ($this->loadingOverrides) {
      // Prevent recursion. Which may happen like that:
      // - LocaleConfigManagerExcerpt::getDefaultConfigLangcode checks if a
      //   given config is a config entity.
      // - Entity definitions are collected.
      // - Any entity definition depends on config
      //   (e.g. \commerce_order_entity_type_alter)
      // Fortunately we can assume that translation overrides do not affect
      // config entity prefix definitions in any sane way.
      return [];
    }
    $targetLangcode = $this->getTargetLanguage()->getId();
    $overrides = [];
    $this->loadingOverrides = TRUE;
    foreach ($names as $name) {
      $overrides[$name] = $this->getConfigTranslator()->getTranslatableConfig($name, $targetLangcode);
    }
    $this->loadingOverrides = FALSE;
    return $overrides;
  }

  /**
   * Create config object.
   *
   * Nothing needed here, this is completely dynamic.
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION): ?StorableConfigBase {
    return NULL;
  }

  /**
   * Get cache suffix.
   */
  public function getCacheSuffix(): ?string {
    return $this->getTargetLanguage()->getId();
  }

  /**
   * Get cacheability.
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    $metadata = new CacheableMetadata();
    if ($this->getConfigTranslator()->hasTranslatableConfig($name)) {
      $metadata->setCacheContexts(['languages:language_interface']);
    }
    return $metadata;
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\ConfigOverride;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\translation_bliss\ConfigExclude\TranslationBlissConfigExcludeInterface;
use Drupal\translation_bliss\SourceLanguage\SrcLang;
use Drupal\translation_bliss_config\Utility\ArrayPathAccess;
use Drupal\translation_bliss_config\Utility\TypedConfigPathHelper;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\AutowireServiceClosure;

/**
 * Config translations override provider.
 *
 * Copied and adapted some methods from LocaleConfigManager.
 *
 * @see \Drupal\locale\LocaleConfigManager
 *
 * @internal
 */
final class TranslationBlissConfigTranslator {

  /**
   * @param \Closure(): \Drupal\Core\Config\StorageInterface $getConfigStorage
   * @param \Closure(): \Drupal\Core\Config\TypedConfigManagerInterface $getTypedConfigManager
   */
  public function __construct(
    #[AutowireServiceClosure('config.storage')]
    protected \Closure $getConfigStorage,
    #[AutowireServiceClosure(TypedConfigManagerInterface::class)]
    protected \Closure $getTypedConfigManager,
    protected TranslationBlissConfigExcludeInterface $translationBlissConfigExclude,
    #[Autowire(service: 'string_translation')] // Only aliased as TranslationInterface.
    protected TranslationInterface&TranslatorInterface $translationManager,
  ) {}

  /**
   * Check if config has translatable.
   */
  public function hasTranslatableConfig(string $name): bool {
    $data = ($this->getConfigStorage)()->read($name);
    $result = FALSE;
    if ($data) {
      $typed_config = ($this->getTypedConfigManager)()->createFromNameAndData($name, $data);
      /** @noinspection PhpLoopNeverIteratesInspection */
      foreach ($this->iterateTranslatableElements($typed_config) as $_) {
        $result = TRUE;
        break;
      }
    }
    return $result;
  }

  /**
   * Adapted @see \Drupal\locale\LocaleConfigManager::getTranslatableDefaultConfig
   *
   * In contrast to that, config is used, not module provided config.
   *
   * @return array
   */
  public function getTranslatableConfig(string $name, string $targetLangcode) {
    $data = ($this->getConfigStorage)()->read($name);
    $translatableData = [];
    if ($data) {
      $typed_config = ($this->getTypedConfigManager)()->createFromNameAndData($name, $data);
      $sourceLangcode = $data['langcode'] ?? 'en';
      if ($sourceLangcode !== $targetLangcode) {
        $translatable = new ArrayPathAccess();
        $hasTranslatable = FALSE;
        foreach ($this->iterateTranslatableElements($typed_config) as $element) {
          $hasTranslatable = TRUE;
          $value = $element->getValue();
          $context = $definition['translation context'] ?? '';
          $context = SrcLang::create($sourceLangcode)->context($context);
          $maybeTranslation = $this->translationManager
            ->getStringTranslation($targetLangcode, $value, $context);
          if ($maybeTranslation) {
            $path = (new TypedConfigPathHelper($element))->getRootRelativePropertyPath();
            $translatable->set($path, $maybeTranslation);
          }
        }
        if ($hasTranslatable) {
          $translatable->set('langcode', $targetLangcode);
        }
        $translatableData = $translatable->getValue();
        assert(is_array($translatableData));
      }
    }
    return $translatableData;
  }

  /**
   * Iterate translatable paths.
   *
   * Adapted from @see \Drupal\locale\LocaleConfigManager::getTranslatableData
   *
   * @return \Generator<TypedDataInterface>
   */
  protected function iterateTranslatableElements(TypedDataInterface $element): \Generator {
    $translatable = [];
    if ($element instanceof TraversableTypedDataInterface) {
      foreach ($element as $key => $property) {
        yield from $this->iterateTranslatableElements($property);
      }
    }
    else {
      // Something is only translatable by Locale if there is a string in the
      // first place.
      $value = $element->getValue();
      $definition = $element->getDataDefinition();
      if (
        !empty($definition['translatable'])
        && $value !== ''
        && $value !== NULL
        && !$this->translationBlissConfigExclude?->isConfigTranslationExcluded($element)
      ) {
        yield $element;
      }
    }
    return $translatable;
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\translation_bliss_config\CoreOverride\LocaleConfigManager\TranslationBlissLocaleConfigManagerTrait;
use geeks4change\composable_inheritance\ComposableInheritance;

/**
 * @internal
 */
final class TranslationBlissConfigServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    // Disable locale config subscriber.
    $localeConfigSubscriberDefinition = $container->getDefinition('locale.config_subscriber');
    $localeConfigSubscriberDefinition->clearTag('event_subscriber');

    // Override locale config manager.
    // The replacement trivially disables one method that has no self-calls in
    // the class. It can not be decorated and MUST be subclassed, because it has
    // no interface and other services typehint the class.
    /* @see \Drupal\locale\LocaleConfigManager::updateConfigTranslations */
    /* @see \Drupal\locale\LocaleConfigSubscriber::$localeConfigManager */
    $localeConfigManagerDefinition = $container->getDefinition('locale.config_manager');
    $class = ComposableInheritance::create($localeConfigManagerDefinition->getClass())
      ->useTrait(TranslationBlissLocaleConfigManagerTrait::class)
      ->class();
    $localeConfigManagerDefinition->setClass($class);
  }

}

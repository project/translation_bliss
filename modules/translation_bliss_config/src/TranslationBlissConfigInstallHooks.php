<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config;

use Drupal\Core\Installer\InstallerKernel;

/**
 * TranslationBliss config install hooks.
 *
 * Removes sync of interface translations and config overrides.:
 * - locale_system_set_config_langcodes sets all default config language to
 *   current default language, but only if that is not english.
 * - locale_system_update downloads translations AND syncs to config.
 *   Omit the latter, do the former in _translation_bliss_download.
 *
 * @see \_translation_bliss_download
 * @see \locale_modules_installed
 * @see \locale_themes_installed
 * @see \locale_system_set_config_langcodes
 * @see \locale_system_update
 * @see \Drupal\locale\LocaleConfigManager::updateConfigTranslations
 *
 * @internal
 */
final class TranslationBlissConfigInstallHooks {

  /**
   * @see \translation_bliss_config_module_implements_alter
   */
  function moduleImplementsAlter(array &$implementations, string $hook) {
    if (in_array($hook, ['modules_installed', 'themes_installed'])) {
      unset($implementations['locale']);
    }
  }

  /**
   * @see \translation_bliss_modules_installed()
   */
  function modulesInstalled(array $modules, bool $is_syncing) {
    $this->downloadComponentTranslations(['module' => $modules]);
  }

  /**
   * @see \translation_bliss_themes_installed
   */
  function themesInstalled(array $themes) {
    $this->downloadComponentTranslations(['theme' => $themes]);
  }

  /**
   * Download translations on module / theme install.
   *
   * Copied the wanted parts from locale_system_update.
   *
   * @see locale_system_update
   */
  private function downloadComponentTranslations(array $components) {
    $components += ['module' => [], 'theme' => []];
    $list = array_merge($components['module'], $components['theme']);

    // Skip running the translation imports if in the installer,
    // because it would break out of the installer flow. We have
    // built-in support for translation imports in the installer.
    if (!InstallerKernel::installationAttempted() && locale_translatable_language_list()) {
      if (\Drupal::config('locale.settings')->get('translation.import_enabled')) {
        \Drupal::moduleHandler()->loadInclude('locale', 'compare.inc');

        // Update the list of translatable projects and start the import batch.
        // Only when new projects are added the update batch will be triggered.
        // Not each enabled module will introduce a new project. E.g. sub modules.
        $projects = array_keys(locale_translation_build_projects());
        if ($list = array_intersect($list, $projects)) {
          \Drupal::moduleHandler()->loadInclude('locale', 'fetch.inc');
          // Get translation status of the projects, download and update
          // translations.
          $options = _locale_translation_default_update_options();
          $batch = locale_translation_batch_update_build($list, [], $options);
          batch_set($batch);
        }
      }
      // OMITTED updating config.
    }
  }

}

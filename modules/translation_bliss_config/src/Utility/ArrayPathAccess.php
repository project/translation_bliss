<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\Utility;

/**
 * @internal
 */
final class ArrayPathAccess {

  public function __construct(
    private mixed $value = [],
  ) {}

  public function getValue(): mixed {
    return $this->value;
  }

  public function set(string $path, mixed $value): void {
    $keys = $path ? explode('.', $path) : [];
    $current =& $this->value;
    foreach ($keys as $key) {
      if (!is_array($current)) {
        $current = [];
      }
      $current =& $current[$key];
    }
    $current = $value;
  }

}

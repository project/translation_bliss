<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\Utility;

use Drupal\Core\TypedData\TypedDataInterface;

/**
 * @internal
 */
final class TypedConfigPathHelper {

  public function __construct(
    private readonly TypedDataInterface $element,
  ) {}

  public function getRootRelativePropertyPath(): string {
    $rootPath = $this->element->getRoot()->getPropertyPath();
    $path = $this->element->getPropertyPath();
    assert(str_starts_with($path, "$rootPath."));
    return substr($path, strlen($rootPath) + 1);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\CoreOverride\LocaleConfigManager;

use Drupal\locale\LocaleConfigManager;

/**
 * This class is not used. It tells the IDE about ComposableInheritance.
 *
 * @see \Drupal\translation_bliss\TranslationBlissTranslationsServiceProvider::alter
 * @internal
 */
final class TranslationBlissLocaleConfigManager extends LocaleConfigManager {

  use TranslationBlissLocaleConfigManagerTrait;

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_config\CoreOverride\LocaleConfigManager;

/**
 * Trait to be used by ComposableInheritance.
 *
 * @see \Drupal\translation_bliss_config\TranslationBlissConfigServiceProvider::alter
 *
 * @internal
 */
trait TranslationBlissLocaleConfigManagerTrait {

  public function updateConfigTranslations(array $names, array $langcodes = []) {
    return 0;
  }

}

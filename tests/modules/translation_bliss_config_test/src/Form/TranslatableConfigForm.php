<?php

declare(strict_types=1);

namespace Drupal\translation_bliss_config_test\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\RedundantEditableConfigNamesTrait;

/**
 * Configure Translation bliss config test settings for this site.
 */
final class TranslatableConfigForm extends ConfigFormBase {

  use RedundantEditableConfigNamesTrait;

  public function getFormId(): string {
    return 'translation_bliss_config_test_translatable_config';
  }

  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['t_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String'),
      '#config_target' => 'translation_bliss.translatable:string',
    ];
    $form['t_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#config_target' => 'translation_bliss.translatable:label',
    ];
    $form['s_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String'),
      '#config_target' => 'translation_bliss.srclang:string',
    ];
    $form['s_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#config_target' => 'translation_bliss.srclang:label',
    ];
    return parent::buildForm($form, $form_state);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss_test_language_manager;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManager;

final class TestLanguageManager extends LanguageManager {

  protected ?LanguageInterface $currentLanguage;

  public function setCurrentLanguage(?LanguageInterface $currentLanguage): void {
    $this->currentLanguage = $currentLanguage;
  }

  public function getCurrentLanguage($type = LanguageInterface::TYPE_INTERFACE) {
    return $this->currentLanguage
      ?? parent::getCurrentLanguage($type);
  }

}

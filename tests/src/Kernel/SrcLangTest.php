<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Kernel;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\KernelTests\KernelTestBase;
use Drupal\translation_bliss\SourceLanguage\SrcLangStringTranslationTrait;

final class SrcLangTest extends KernelTestBase {

  use SrcLangStringTranslationTrait;

  protected static $modules = [
    'translation_bliss',
    'locale',
  ];

  /**
   * @dataProvider provideSourceLanguage
   */
  public function testSourceLanguage(string $srcLang, string $context, string $expectedContext): void {

    $markup = $this->tFrom(
      $srcLang,
      'Some @test',
      ['@text' => 'Text'],
      ['some_option' => 123] + ($context ? ['context' => $context] : [])
    );
    assert($markup instanceof TranslatableMarkup);

    $expected = new TranslatableMarkup(
      'Some @test',
      ['@text' => 'Text'],
      ['some_option' => 123] + ($expectedContext ? ['context' => $expectedContext] : [])
    );
    $this->assertEquals($expected, $markup);
  }

  public static function provideSourceLanguage() {
    yield ['en', '' , ''];
    yield ['en', 'myContext' , 'myContext'];
    yield ['de', '' , '[srclang=de]'];
    yield ['de', 'myContext' , '[srclang=de]myContext'];
  }

}

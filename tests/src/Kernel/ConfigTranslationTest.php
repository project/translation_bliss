<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Kernel;

use Drupal\Core\Language\Language;
use Drupal\KernelTests\KernelTestBase;
use Drupal\translation_bliss_test_language_manager\TestLanguageManager;

final class ConfigTranslationTest extends KernelTestBase {

  protected static $modules = [
    'locale',
    'translation_bliss_config',
    'translation_bliss',
    'translation_bliss_config_test',
    'translation_bliss_test_language_manager',
  ];

  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('locale', ['locales_source', 'locales_target', 'locales_location', 'locale_file']);
    $this->installConfig('translation_bliss_config_test');
    /** @see settings.php */
    /** @see \Drupal\Core\StringTranslation\Translator\CustomStrings */
    $this->setSetting('locale_custom_strings_de', [
      /* context = */ '' => [
        'Value' => 'Wert',
      ],
    ]);
    $this->setSetting('locale_custom_strings_en', [
      /* context = */ '[srclang=de]' => [
        'Wert' => 'Value',
      ],
    ]);
  }

  /**
   * @dataProvider provideTranslationBlissConfig
   */
  public function testTranslationBlissConfig(
    string $configName,
    string $langcode,
    array $expectedData,
    bool $hasLanguageCacheability,
  ): void {
    $languageManager = \Drupal::languageManager();
    // No language module, so LanguageManager, not ConfigurableLanguageManager.
    assert($languageManager instanceof TestLanguageManager);
    $languageManager->setCurrentLanguage(new Language(['id' => $langcode]));

    $config = \Drupal::configFactory()->get($configName);
    $configData = $config->get();
    unset($configData['_core']);
    $this->assertSame($expectedData, $configData);

    $this->assertSame($hasLanguageCacheability, in_array('languages:language_interface', $config->getCacheContexts()));
  }

  public static function provideTranslationBlissConfig() {
    yield ['translation_bliss.untranslatable', 'en', ['string' => 'Value'], FALSE];
    yield ['translation_bliss.untranslatable', 'de', ['string' => 'Value'], FALSE];

    yield ['translation_bliss.translatable', 'en', ['langcode' => 'en', 'string' => 'Value', 'label' => 'Value'], TRUE];
    yield ['translation_bliss.translatable', 'de', ['langcode' => 'de', 'string' => 'Value', 'label' => 'Wert'], TRUE];

    yield ['translation_bliss.srclang', 'de', ['langcode' => 'de', 'string' => 'Wert', 'label' => 'Wert'], TRUE];
    yield ['translation_bliss.srclang', 'en', ['langcode' => 'en', 'string' => 'Wert', 'label' => 'Value'], TRUE];
  }

}

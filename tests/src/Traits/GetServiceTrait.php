<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Traits;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;

/**
 * Get services from global container to not have container lost.
 */
trait GetServiceTrait {

  protected function getModuleInstaller(): ModuleInstallerInterface {
    return \Drupal::service(ModuleInstallerInterface::class);
  }

  protected function getModuleHandler(): ModuleHandlerInterface {
    return \Drupal::service(ModuleHandlerInterface::class);
  }

  protected function getConfigFactory(): ConfigFactoryInterface {
    return \Drupal::configFactory();
  }

  protected function getLanguageConfigFactoryOverride(): LanguageConfigFactoryOverrideInterface {
    return \Drupal::service(LanguageConfigFactoryOverrideInterface::class);
  }

}

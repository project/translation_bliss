<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Traits;

use Drupal\Core\Database\Database;

trait FixDriverTrait {

  protected function installParameters() {
    // Strange that we need this...
    /** @see \Drupal\Core\Test\FunctionalTestSetupTrait::installParameters */
    // Otherwise we get: Undefined array key "driver" in install.core.inc:973
    /** @see \install_get_form */
    $parameters = parent::installParameters();
    $parameters['forms']['install_settings_form']['driver']
      = Database::getConnectionInfo()['default']['namespace'];
    return $parameters;
  }

}

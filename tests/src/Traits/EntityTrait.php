<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Traits;

use Drupal\Core\Entity\EntityInterface;

trait EntityTrait {

  protected function reloadEntity(EntityInterface $entity) {
    $controller = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id()]);
    return $controller->load($entity->id());
  }

}

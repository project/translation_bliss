<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Traits;

use Drupal\locale\StringInterface;
use Drupal\locale\StringStorageInterface;
use Drupal\locale\TranslationString;

trait TranslationsTrait {

  protected StringStorageInterface $localeStorage;

  public function getTestTranslationsDir(): string {
    $translationsDir = \Drupal::moduleHandler()
        ->getModule('translation_bliss')
        ->getPath() . '/tests/translations';
    return $translationsDir;
  }

  protected function addOrUpdateTranslation(string $langcode, string $translation, string $source, string $context = ''): TranslationString {
    $translationString = $this->getLocaleStorage()->findTranslation([
      'source' => $source,
      'context' => $context,
    ]);
    if ($translationString) {
      $translationString->setString($translation)->save();
    }
    else {
      $sourceString = $this->getLocaleStorage()->findString([
        'source' => $source,
        'context' => $context,
      ]);
      if (!$sourceString) {
        $sourceString = $this->getLocaleStorage()->createString([
          'source' => $source,
          'context' => $context,
        ])->save();
      }
      $translationString = $this->getLocaleStorage()->createTranslation([
        'lid' => $sourceString->lid,
        'language' => $langcode,
        'translation' => $translation,
      ])->save();
    }
    return $translationString;
  }

  protected function getTranslation(string $source, string $context = ''): ?TranslationString {
    return $this->getLocaleStorage()->findTranslation([
      'source' => $source,
      'context' => $context,
    ])
    // Drop storage, as phpunit coughs on serializing the connection.
    ?->setStorage(NULL);
  }

  protected function getAllTranslations(string $langcode = ''): array {
    $options = [];
    if ($langcode) {
      $options['language'] = $langcode;
    }
    return array_map(
    // Drop storage, as phpunit coughs on serializing the connection.
      fn(StringInterface $string) => $string->setStorage(NULL),
      $this->getLocaleStorage()->getTranslations([], $options),
    );
  }

  protected function getLocaleStorage(): StringStorageInterface {
    return \Drupal::service('locale.storage');
  }

}

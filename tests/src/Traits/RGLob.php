<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Traits;

final class RGLob {

  public function __construct(
    private readonly string $dir,
    private readonly string $regex = '/.*/',
  ) {
    assert(file_exists($dir));
    assert(is_dir($dir));
    assert(preg_match($this->regex, '') !== FALSE);
  }

  /**
   * @return list<string>
   */
  public function paths(): array {
    $dir = new \RecursiveDirectoryIterator($this->dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS);
    $files = new \RecursiveIteratorIterator($dir);
    $filtered = new \RegexIterator($files, $this->regex, \RegexIterator::MATCH);
    $paths = array_keys(iterator_to_array($filtered));
    sort($paths);
    return $paths;
  }

}

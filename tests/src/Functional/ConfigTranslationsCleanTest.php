<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Functional;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\locale\LocaleDefaultConfigStorage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;
use Drupal\Tests\translation_bliss\Traits\GetServiceTrait;
use Drupal\Tests\translation_bliss\Traits\TranslationsTrait;
use Drupal\translation_bliss\Drush\Commands\Config\ConfigTranslationCommand;
use Drupal\translation_bliss\Drush\Commands\Translation\TranslationsImportCommands;
use Drush\Commands\pm\PmCommands;
use Drush\TestTraits\DrushTestTrait;

final class ConfigTranslationsCleanTest extends BrowserTestBase {

  use FixDriverTrait;
  use DrushTestTrait;
  use TranslationsTrait;
  use GetServiceTrait;

  protected ModuleInstallerInterface $moduleInstaller;
  protected ConfigFactoryInterface $configFactory;
  protected LanguageConfigFactoryOverrideInterface $languageConfigFactoryOverride;

  protected static $modules = [
    'language',
    'locale',
    'translation_bliss',
  ];

  protected $defaultTheme = 'stark';

  protected function setUp(): void {
    parent::setUp();
    $this->moduleInstaller = \Drupal::service(ModuleInstallerInterface::class);
    $this->configFactory = \Drupal::service(ConfigFactoryInterface::class);
    $this->languageConfigFactoryOverride = \Drupal::service(LanguageConfigFactoryOverrideInterface::class);
    ConfigurableLanguage::createFromLangcode('de')->save();
    // Add some translations.
    $this->drush(TranslationsImportCommands::TranslationsImportCommand, [$this->getTestTranslationsDir()]);
  }

  public function testConfigTranslationCleanCommand(): void {
    $this->installTestConfig();
    $this->verifyTestConfig();
    // Test config translations exist because syncInterfaceTOConfig.
    $this->verifyTestConfigTranslations(exists: TRUE);

    // Install now to prevent syncConfigToInterface.
    $this->installModule('translation_bliss_config');

    // Add real config translation override to two.
    $this->getLanguageConfigFactoryOverride()
      ->getOverride('de', 'translation_bliss_config_override_test.two')
      ->set('nested.string', 'A real translation')
      ->save();

    $this->drush(ConfigTranslationCommand::ConfigTranslationsCleanCommand);

    // Verify that only two is left.
    $this->verifyTestConfigTranslation('one', exists: FALSE);
    $this->verifyTestConfigTranslation('two', exists: TRUE);
  }

  /**
   * Test that syncing interface to config is prevented by us.
   *
   * @see \Drupal\locale\LocaleConfigManager::updateConfigTranslations
   * @see \Drupal\translation_bliss_config\TranslationBlissConfigServiceProvider
   *
   * @testWith [false]
   *           [true]
   */
  public function testSyncInterfaceToConfigTranslations(bool $suppressSync): void {
    if ($suppressSync) {
      $this->installModule('translation_bliss_config');
    }
    $this->installTestConfig();
    $this->verifyTestConfig();
    $this->verifyTestConfigTranslations(exists: !$suppressSync);
  }

  /**
   * Test that syncing config to interface is prevented by us.
   *
   * @see \Drupal\locale\LocaleConfigSubscriber::onConfigSave
   * @see \Drupal\translation_bliss_config\TranslationBlissConfigServiceProvider
   *
   * @testWith [false]
   *           [true]
   */
  public function testSyncConfigToInterfaceTranslations(bool $suppressSync): void {
    $this->installTestConfig();
    $this->verifyTestConfig();

    $localeDeraultConfigStorage = \Drupal::service(LocaleDefaultConfigStorage::class);
    $this->assertNotEmpty($localeDeraultConfigStorage->read('translation_bliss_config_override_test.two'));


    $this->assertNotEmpty(\Drupal::service(LocaleDefaultConfigStorage::class)->read('translation_bliss_config_override_test.two'));
    $this->verifyTestConfigTranslations(exists: TRUE);
    if ($suppressSync) {
      $this->installModule('translation_bliss_config');
    }
    $this->assertNotEmpty(\Drupal::service(LocaleDefaultConfigStorage::class)->read('translation_bliss_config_override_test.two'));
    $overrideTwoData = $this->getLanguageConfigFactoryOverride()
      ->getOverride('de', 'translation_bliss_config_override_test.two')
      ->set('nested.string', 'Eine andere Übersetzung')
      ->save();
    $this->assertNotEmpty(\Drupal::service(LocaleDefaultConfigStorage::class)->read('translation_bliss_config_override_test.two'));
    $translationString = $this->getTranslation('Some custom translated string')->getString();
    if ($suppressSync) {
      $this->assertSame('Ein projektspezifisch übersetzter Eintrag', $translationString);
    }
    else {
      $this->assertSame('Eine andere Übersetzung', $translationString);
    }
  }

  protected function installTestConfig(): void {
    // Install our test config module.
    // Use drush to have locale batch sync executed.
    $this->installModule('translation_bliss_config_override_test');
  }

  /**
   * (Alt) Install a module, run batch, and get a correct container.
   */
  public function _installModule(string $module): void {
    $this->drush(PmCommands::INSTALL, [$module], ['yes' => TRUE]);

    \Drupal::service('kernel')->resetContainer();
    $this->container = \Drupal::getContainer();
  }

  /**
   * Install a module, run batch, and get a correct container.
   */
  public function installModule(string $module): void {
    \Drupal::service('module_installer')->install([$module]);
    if ($batch =& batch_get()) {
      $batch['progressive'] = FALSE;
      batch_process();
    }
    $this->assertEmpty(batch_get());
    \Drupal::service('kernel')->resetContainer();
    $this->container = \Drupal::getContainer();
  }

  protected function verifyTestConfig(): void {
    $this->assertNotEmpty($this->getConfigFactory()
      ->get('translation_bliss_config_override_test.one')->getRawData());
    $this->assertNotEmpty($this->getConfigFactory()
      ->get('translation_bliss_config_override_test.two')->getRawData());
  }

  protected function verifyTestConfigTranslations(bool $exists): void {
    $this->verifyTestConfigTranslation('one', $exists);
    $this->verifyTestConfigTranslation('two', $exists);
  }

  public function verifyTestConfigTranslation(string $item, bool $exists): void {
    $translationData = $this->getLanguageConfigFactoryOverride()
      ->getOverride('de', "translation_bliss_config_override_test.$item")
      ->getRawData();
    if ($exists) {
      $this->assertNotEmpty($translationData);
    }
    else {
      $this->assertEmpty($translationData);
    }
  }

}

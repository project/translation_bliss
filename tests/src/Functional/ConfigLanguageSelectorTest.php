<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\EntityTrait;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;
use Drupal\user\Entity\Role;

/**
 * @covers \Drupal\translation_bliss_config_language_select\Hooks\ConfigLanguageSelectorHooks
 */
final class ConfigLanguageSelectorTest extends BrowserTestBase {

  use FixDriverTrait;
  use EntityTrait;

  protected static $modules = [
    'translation_bliss_config_language_select',
    'language',
    'translation_bliss_config_test',
  ];

  protected $defaultTheme = 'stark';

  public function testConfigEntityLanguageSelector() {
    ConfigurableLanguage::createFromLangcode('de')->save();
    ConfigurableLanguage::createFromLangcode('es')->save();
    $this->drupalLogin($this->rootUser);

    // Add role, set 'de'.
    $this->drupalGet('/admin/people/roles/add');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->optionExists('langcode', 'en');
    $this->assertSession()->optionExists('langcode', 'de');
    $this->assertSession()->optionExists('langcode', 'es');

    $this->submitForm([
      'id' => 'test_role',
      'label' => 'Test role',
      'langcode' => 'de',
    ], 'Save');
    $this->assertSession()->statusCodeEquals(200);

    // Verify.
    $role = Role::load('test_role');
    $this->assertSame('de', $role->language()->getId());

    // Edit role, set 'es'.
    $this->drupalGet('/admin/people/roles/manage/test_role');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->optionExists('langcode', 'en');
    $this->assertSession()->optionExists('langcode', 'de');
    $this->assertSession()->optionExists('langcode', 'es');

    $this->submitForm([
      'id' => 'test_role',
      'label' => 'Test role',
      'langcode' => 'es',
    ], 'Save');
    $this->assertSession()->statusCodeEquals(200);

    // Verify.
    $role = $this->reloadEntity($role);
    $this->assertSame('es', $role->language()->getId());

    // Delete role.
    $this->drupalGet('/admin/people/roles/manage/test_role/delete');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldNotExists('langcode');
  }

  public function testConfigMultiFormLanguageSelector() {
    ConfigurableLanguage::createFromLangcode('de')->save();
    ConfigurableLanguage::createFromLangcode('es')->save();
    $this->drupalLogin($this->rootUser);

    // For multi config form, set 'de' / 'es'.
    $this->drupalGet('/admin/config/system/translatable-config');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->optionExists('langcode__translation_bliss__translatable', 'en');
    $this->assertSession()->optionExists('langcode__translation_bliss__translatable', 'de');
    $this->assertSession()->optionExists('langcode__translation_bliss__translatable', 'es');

    $this->assertSession()->optionExists('langcode__translation_bliss__srclang', 'en');
    $this->assertSession()->optionExists('langcode__translation_bliss__srclang', 'de');
    $this->assertSession()->optionExists('langcode__translation_bliss__srclang', 'es');

    $this->assertSession()->fieldNotExists('langcode__translation_bliss__untranslatable');

    $this->submitForm([
      'langcode__translation_bliss__translatable' => 'de',
      'langcode__translation_bliss__srclang' => 'es',
    ], 'Save');
    $this->assertSession()->statusCodeEquals(200);

    // Verify.
    $config = $this->config('translation_bliss.translatable');
    $this->assertSame('de', $config->get('langcode'));

    $config = $this->config('translation_bliss.srclang');
    $this->assertSame('es', $config->get('langcode'));
  }

}

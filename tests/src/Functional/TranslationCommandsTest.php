<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;
use Drupal\Tests\translation_bliss\Traits\TranslationsTrait;
use Drupal\translation_bliss\Drush\Commands\Translation\TranslationsImportCommands;
use Drush\TestTraits\DrushTestTrait;

final class TranslationCommandsTest extends BrowserTestBase {

  use FixDriverTrait;
  use DrushTestTrait;
  use TranslationsTrait;

  protected static $modules = [
    'translation_bliss',
    'language',
    'locale',
  ];

  protected $defaultTheme = 'stark';

  protected function setUp(): void {
    parent::setUp();

    ConfigurableLanguage::createFromLangcode('de')->save();

    // Some string not in imported translations.
    $this->addOrUpdateTranslation('de', 'Glubberiglubsch', 'Gobbeldigok');
    $this->addOrUpdateTranslation('de', 'Glubsch', 'Gobbeldi');
  }

  public function testTranslationsRebuild(): void {
    $translationsDir = $this->getTestTranslationsDir();

    $this->assertNotEmpty($this->getLocaleStorage()->countStrings());
    $this->assertNotEmpty($this->getLocaleStorage()->countTranslations()['de'] ?? NULL);

    $translationStringToRemove = $this->getTranslation('Gobbeldigok');
    $this->assertNotNull($translationStringToRemove);
    $this->assertNotNull($translationStringToRemove->translation);

    $this->drush(TranslationsImportCommands::TranslationsImportCommand, [$translationsDir]);

    // Test that the translation is deleted, but not the source.
    $translationStringToRemove = $this->getTranslation('Gobbeldigok');
    $this->assertNotNull($translationStringToRemove);
    $this->assertNull($translationStringToRemove->translation);

    $contribTranslation = $this->getTranslation('Anonymous');
    $this->assertNotNull($contribTranslation);
    $this->assertSame('Anonym', $contribTranslation->getString());
    $this->assertEmpty($contribTranslation->customized);

    $customTranslation = $this->getTranslation('Some custom translated string');
    $this->assertNotNull($customTranslation);
    $this->assertSame('Ein projektspezifisch übersetzter Eintrag', $customTranslation->getString());
    $this->assertNotEmpty($customTranslation->customized);

    $overriddenTranslation = $this->getTranslation('Authenticated');
    $this->assertNotNull($overriddenTranslation);
    $this->assertSame('Voll angemeldet', $overriddenTranslation->getString());
    $this->assertNotEmpty($overriddenTranslation->customized);
  }

}

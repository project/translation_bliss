<?php

namespace Drupal\Tests\translation_bliss\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;

/**
 * Simple browser test.
 *
 * @group entity_unified_access
 */
class AdminPageTest extends BrowserTestBase {

  use FixDriverTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'translation_bliss',
    'translation_bliss_config',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the /admin page returns a 200.
   */
  public function testAdminPage() {
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertTrue(TRUE);
  }

}

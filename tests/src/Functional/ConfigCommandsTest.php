<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;
use Drupal\translation_bliss\Drush\Commands\Config\ConfigCommands;
use Drush\TestTraits\DrushTestTrait;

final class ConfigCommandsTest extends BrowserTestBase {

  use FixDriverTrait;
  use DrushTestTrait;

  protected static $modules = [
    'translation_bliss',
    'language',
  ];

  protected $defaultTheme = 'stark';

  public function testLanguageSummaryAndDetails(): void {
    $this->drush(ConfigCommands::ConfigLanguageSummaryCommand, options: ['format' => 'json']);
    $data = $this->getOutputFromJSON();

    $this->assertIsArray($data);
    $itemCount = $data[0]['count'] ?? NULL;
    $this->assertIsInt($itemCount);
    $this->assertGreaterThan(0, $itemCount);
    $this->assertSame([
      [
        'language' => 'en',
        'count' => $itemCount,
      ],
    ], $data);

    $this->drush(ConfigCommands::ConfigLanguageDetailsCommand, options: ['format' => 'json']);
    $data = $this->getOutputFromJSON();

    $this->assertIsArray($data);
    $this->assertCount($itemCount, $data);
    foreach ($data as $item) {
      $this->assertIsArray($item);
      $itemName = $item['name'] ?? NULL;
      $this->assertSame([
        'language' => 'en',
        'name' => $itemName,
      ], $item);
    }
  }

  public function testLanguageConvertOne(): void {
    ConfigurableLanguage::createFromLangcode('de')->save();

    $this->drush(ConfigCommands::ConfigLanguageConvertCommand, args: ['de', 'user.settings'], options: ['format' => 'json']);
    $data = $this->getOutputFromJSON();
    $this->assertSame([
      [
      'originalLanguage' => 'en',
      'targetLanguage' => 'de',
      'name' => 'user.settings',
      ],
    ], $data);

    $this->assertSame('de', \Drupal::config('user.settings')->get('langcode'));
    $this->assertSame('en', \Drupal::config('system.site')->get('langcode'));
  }

  public function testLanguageConvertAll(): void {
    ConfigurableLanguage::createFromLangcode('de')->save();

    $this->drush(ConfigCommands::ConfigLanguageConvertCommand, args: ['de'], options: ['all' => true, 'format' => 'json']);
    $data = $this->getOutputFromJSON();

    $this->assertIsArray($data);
    $this->assertGreaterThan(1, count($data));

    $name = $data[0]['name'] ?? NULL;
    $this->assertIsString($name);
    $this->assertSame([
      'originalLanguage' => 'en',
      'targetLanguage' => 'de',
      'name' => $name,
    ], $data[0]);

    $this->assertSame('de', \Drupal::config('user.settings')->get('langcode'));
    $this->assertSame('de', \Drupal::config('system.site')->get('langcode'));
  }

}

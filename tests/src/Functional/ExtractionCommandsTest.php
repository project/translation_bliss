<?php

declare(strict_types=1);
namespace Drupal\Tests\translation_bliss\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\translation_bliss\Traits\FixDriverTrait;
use Drupal\Tests\translation_bliss\Traits\RGLob;
use Drupal\translation_bliss\Drush\Commands\Extraction\TranslationExtractionCommands;
use Drush\TestTraits\DrushTestTrait;

final class ExtractionCommandsTest extends BrowserTestBase {

  use FixDriverTrait;
  use DrushTestTrait;

  protected static $modules = [
    'translation_bliss',
    'potx',
    'language',
    // Must be installed to extract config.
    'translation_bliss_potx_test',
  ];

  protected $defaultTheme = 'stark';

  public function testCustomStringExtraction(): void {
    ConfigurableLanguage::createFromLangcode('de')
      ->save();

    // Set up some custom config.
    $this->config('translation_bliss_potx_test.settings')
      ->set('string', 'Custom config string')
      ->set('label', 'Custom config label')
      ->save();
    $this->config('translation_bliss_potx_test.settings_srclang')
      ->set('langcode', 'de')
      ->set('string', 'Angepasste Config Zeichenkette')
      ->set('label', 'Angepasster Config Bezeichner')
      ->save();

    $this->importTranslations();

    $fs = \Drupal::service(FileSystemInterface::class);
    assert($fs instanceof FileSystemInterface);
    $targetDir = $fs->getTempDirectory() . "/" . $this->randomMachineName();

    $sourceDir = \Drupal::moduleHandler()
      ->getModule('translation_bliss')
      ->getPath() . '/tests/modules/translation_bliss_potx_test';

    // Write untranslatable strings.
    mkdir($targetDir);
    mkdir("$targetDir/adjustments");
    file_put_contents("$targetDir/" . TranslationExtractionCommands::FileCustomStringsUntranslatable, <<<'EOD'
    # Stub untranslateable strings.
    #
    msgid ""
    msgstr ""
    "Project-Id-Version: PROJECT VERSION\n"
    "POT-Creation-Date: 2024-09-09 00:12+1000\n"
    "PO-Revision-Date: 2024-09-09 00:12+1000\n"
    "Last-Translator: NAME <EMAIL@ADDRESS>\n"
    "Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
    "MIME-Version: 1.0\n"
    "Content-Type: text/plain; charset=utf-8\n"
    "Content-Transfer-Encoding: 8bit\n"
    "Plural-Forms: nplurals=2; plural=(n > 1);\n"

    msgid "<br>"
    msgstr ""

    EOD
    );

    $this->drush(TranslationExtractionCommands::TranslationsExtractCommand, options: ['source' => $sourceDir, 'target' => $targetDir]);

    $expectedFiles = [
      $untranslatableStringsTemplateFile = "$targetDir/" . TranslationExtractionCommands::FileCustomStringsUntranslatable,
      $contribTranslatedCommonStringsFile = "$targetDir/" . sprintf(TranslationExtractionCommands::FileContribTranslatedCommonStrings, 'de'),
      $llStringsTemplateFile = "$targetDir/" . TranslationExtractionCommands::FileCustomStringsAllWithOrigins,
      $customStringsFile = "$targetDir/" . sprintf(TranslationExtractionCommands::FileCustomStringsTranslations, 'de'),
      $translatableStringsTemplateFile = "$targetDir/" . TranslationExtractionCommands::FileCustomStringsTranslatable,
      $customTranslatedContribStringsFile = "$targetDir/" . sprintf(TranslationExtractionCommands::FileCustomTranslatedContribStrings, 'de'),
    ];
    $files = (new RGLob($targetDir))->paths();
    $this->assertEquals($expectedFiles, $files);

    $customTranslatedContribStrings = file_get_contents($customTranslatedContribStringsFile);
    $allStringsTemplate = file_get_contents($llStringsTemplateFile);
    $customStrings = file_get_contents($customStringsFile);
    $translatableStringsTemplate = file_get_contents($translatableStringsTemplateFile);
    $untranslatableStringsTemplate = file_get_contents($untranslatableStringsTemplateFile);
    $contribTranslatedCommonStrings = file_get_contents($contribTranslatedCommonStringsFile);

    // Verify that source annotations are only in .pot.
    $this->assertStringContainsString('#: File:modules/', $allStringsTemplate);
    $this->assertStringNotContainsString('#:', $untranslatableStringsTemplate);
    $this->assertStringNotContainsString('#:', $translatableStringsTemplate);
    $this->assertStringNotContainsString('#:', $customStrings);
    $this->assertStringNotContainsString('#:', $customTranslatedContribStrings);
    $this->assertStringNotContainsString('#:', $contribTranslatedCommonStrings);

    // Verify generated files.
    $this->assertStringContainsString("\n# Generated from:\n", $allStringsTemplate);
    $this->assertStringContainsString("\n# - modules/custom/translation_bliss/tests/modules/translation_bliss_potx_test/translation_bliss_potx_test.module\n", $allStringsTemplate);

    // Verify translation template.
    $this->assertStringContainsString(<<<'eod'
    msgctxt "Translation Bliss"
    msgid "Some string with context"
    msgstr ""

    eod
    , $allStringsTemplate);

    // Verify untranslated.
    $this->assertStringContainsString(<<<'eod'
    msgid "<br>"
    msgstr ""

    eod
      , $allStringsTemplate);

    $this->assertStringContainsString(<<<'eod'
    msgid "<br>"
    msgstr ""

    eod
      , $untranslatableStringsTemplate);

    $this->assertStringNotContainsString(<<<'eod'
    msgid "<br>"
    eod
      , $translatableStringsTemplate);

    $this->assertStringNotContainsString(<<<'eod'
    msgid "<br>"
    eod
      , $customStrings);

    $this->assertStringNotContainsString(<<<'eod'
    msgid "<br>"
    eod
      , $customTranslatedContribStrings);

    $this->assertStringNotContainsString(<<<'eod'
    msgid "<br>"
    eod
      , $contribTranslatedCommonStrings);


    // Verify extracted custom strings.
    $this->assertStringContainsString(<<<'eod'
    msgctxt "Translation Bliss"
    msgid "Some string with context"
    msgstr ""

    eod
    , $customStrings);

    $this->assertStringContainsString(<<<'eod'
    msgid "Some custom translated string"
    msgstr "Ein projektspezifisch übersetzter Eintrag"

    eod
    , $customStrings);


    // Verify overridden translations.
    $this->assertStringContainsString(<<<'eod'
    msgid "Authenticated"
    msgstr "Voll angemeldet"

    eod
    , $customTranslatedContribStrings);


    // Verify contrib translations.
    $this->assertStringContainsString(<<<'eod'
    msgid "Anonymous"
    msgstr "Anonym"

    eod
    , $contribTranslatedCommonStrings);


    // Verify that config translations are processed correctly.
    $this->assertStringContainsString(<<<'eod'
    msgid "Custom config label"
    msgstr ""

    eod
    , $allStringsTemplate);

    $this->assertStringContainsString(<<<'eod'
    msgctxt "[srclang=de]"
    msgid "Angepasster Config Bezeichner"
    msgstr ""

    eod
    , $allStringsTemplate);

    $this->assertStringNotContainsString('msgid "Custom config string" ', $allStringsTemplate);
    $this->assertStringNotContainsString('msgid "Angepasste Config Zeichenkette"', $allStringsTemplate);
    $this->assertStringNotContainsString('msgid "Shipped config', $allStringsTemplate);

    // Clean up.
    $fs->deleteRecursive($targetDir);
  }

  private function importTranslations(): void {
    $translationsDir = \Drupal::moduleHandler()
        ->getModule('translation_bliss')
        ->getPath() . '/tests/translations';
    $this->drush('locale:import-all', args: ["$translationsDir"],
      options: ['type' => 'not-customized', 'override' => 'not-customized']);
    $this->drush('locale:import-all', args: ["$translationsDir/custom"],
      options: ['type' => 'customized', 'override' => 'all']);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\CoreFix;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;

final class StringStorageFix {

  private function __construct(
    protected Connection $connection,
    protected array $options,
  ) {}

  public static function create(): self {
    return new self(\Drupal::database(), []);
  }

  /**
   * Temporary replacement for StringDatabaseStorage::deleteStrings
   *
   * Calling that throws:
   *   Query condition 'lid = 1, 2' must have an array compatible operator.
   *
   * @see \Drupal\locale\StringDatabaseStorage::deleteStrings
   *
   * @todo Upstream this StringDatabaseStorage::deleteStrings bug.
   */
  public function deleteStrings(array $conditions): void {
    $lids = $this->dbStringSelect($conditions, ['fields' => ['lid']])->execute()->fetchCol();
    if ($lids) {
      $this->dbDelete('locales_target', ['lid' => $lids])->execute();
      $this->dbDelete('locales_source', ['lid' => $lids])->execute();
      $this->dbDelete('locales_location', ['sid' => $lids])->execute();
    }
  }

  public function deleteTranslations($conditions) {
    $this->dbDelete('locales_target', $conditions)->execute();
  }

  /**
   * FIXED copy of \Drupal\locale\StringDatabaseStorage::dbDelete
   */
  protected function dbDelete($table, $keys) {
    $query = $this->connection->delete($table, $this->options);
    foreach ($keys as $field => $value) {
      // FIXED: Added array cast and 'IN'.
      $query->condition($field, (array) $value, 'IN');
    }
    return $query;
  }


  /**
   * Plain copy of @see \Drupal\locale\StringDatabaseStorage::dbStringSelect
   */
  protected function dbStringSelect(array $conditions, array $options = []) {
    // Start building the query with source table and check whether we need to
    // join the target table too.
    $query = $this->connection->select('locales_source', 's', $this->options)
      ->fields('s');

    // Figure out how to join and translate some options into conditions.
    if (isset($conditions['translated'])) {
      // This is a meta-condition we need to translate into simple ones.
      if ($conditions['translated']) {
        // Select only translated strings.
        $join = 'innerJoin';
      }
      else {
        // Select only untranslated strings.
        $join = 'leftJoin';
        $conditions['translation'] = NULL;
      }
      unset($conditions['translated']);
    }
    else {
      $join = !empty($options['translation']) ? 'leftJoin' : FALSE;
    }

    if ($join) {
      if (isset($conditions['language'])) {
        // If we've got a language condition, we use it for the join.
        $query->$join('locales_target', 't', "t.lid = s.lid AND t.language = :langcode", [
          ':langcode' => $conditions['language'],
        ]);
        unset($conditions['language']);
      }
      else {
        // Since we don't have a language, join with locale id only.
        $query->$join('locales_target', 't', "t.lid = s.lid");
      }
      if (!empty($options['translation'])) {
        // We cannot just add all fields because 'lid' may get null values.
        $query->fields('t', ['language', 'translation', 'customized']);
      }
    }

    // If we have conditions for location's type or name, then we need the
    // location table, for which we add a subquery. We cast any scalar value to
    // array so we can consistently use IN conditions.
    if (isset($conditions['type']) || isset($conditions['name'])) {
      $subquery = $this->connection->select('locales_location', 'l', $this->options)
        ->fields('l', ['sid']);
      foreach (['type', 'name'] as $field) {
        if (isset($conditions[$field])) {
          $subquery->condition('l.' . $field, (array) $conditions[$field], 'IN');
          unset($conditions[$field]);
        }
      }
      $query->condition('s.lid', $subquery, 'IN');
    }

    // Add conditions for both tables.
    foreach ($conditions as $field => $value) {
      $table_alias = $this->dbFieldTable($field);
      $field_alias = $table_alias . '.' . $field;
      if (is_null($value)) {
        $query->isNull($field_alias);
      }
      elseif ($table_alias == 't' && $join === 'leftJoin') {
        // Conditions for target fields when doing an outer join only make
        // sense if we add also OR field IS NULL.
        $query->condition(($this->connection->condition('OR'))
          ->condition($field_alias, (array) $value, 'IN')
          ->isNull($field_alias)
        );
      }
      else {
        $query->condition($field_alias, (array) $value, 'IN');
      }
    }

    // Process other options, string filter, query limit, etc.
    if (!empty($options['filters'])) {
      if (count($options['filters']) > 1) {
        $filter = $this->connection->condition('OR');
        $query->condition($filter);
      }
      else {
        // If we have a single filter, just add it to the query.
        $filter = $query;
      }
      foreach ($options['filters'] as $field => $string) {
        $filter->condition($this->dbFieldTable($field) . '.' . $field, '%' . $this->connection->escapeLike($string) . '%', 'LIKE');
      }
    }

    if (!empty($options['pager limit'])) {
      $query = $query->extend(PagerSelectExtender::class)->limit($options['pager limit']);
    }

    return $query;
  }


}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\DrushFix;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drush\Log\DrushLoggerManager;

final class LocaleCommandsFix {

  private function __construct(
    private DrushLoggerManager $logger,
    private ModuleHandlerInterface $moduleHandler,
    private LanguageManagerInterface $languageManager,
  ) {}

  public static function create(DrushLoggerManager $logger): self {
    return new self($logger, \Drupal::moduleHandler(), \Drupal::languageManager());
  }

  protected function getModuleHandler(): ModuleHandlerInterface
  {
    return $this->moduleHandler;
  }

  protected function getLanguageManager(): LanguageManagerInterface
  {
    return $this->languageManager;
  }

  protected function logger(): DrushLoggerManager
  {
    return $this->logger;
  }

  /**
   * A copy of locale:import-all that returns the result.
   *
   * @see \Drush\Commands\core\LocaleCommands::importAll
   */
  public function importAll(string $directory, string $type, string $override)
  {
    $options = ['type' => $type, 'override' => $override];

    if (!is_dir($directory)) {
      throw new \Exception('The defined directory does not exist.');
    }

    // Look for .po files in defined directory
    $poFiles = glob($directory . DIRECTORY_SEPARATOR . '*.po');
    if (empty($poFiles)) {
      throw new \Exception('Translation files not found in the defined directory.');
    }

    $this->getModuleHandler()->loadInclude('locale', 'translation.inc');
    $this->getModuleHandler()->loadInclude('locale', 'bulk.inc');

    $translationOptions = _locale_translation_default_update_options();
    $translationOptions['customized'] = $this->convertCustomizedType($options['type']);
    $override = $this->convertOverrideOption($options['override']);
    if ($override) {
      $translationOptions['overwrite_options'] = $override;
    }
    $langcodes_to_import = [];
    $files = [];
    foreach ($poFiles as $file) {
      // Ensure we have the file intended for upload.
      if (!file_exists($file)) {
        $this->logger()->warning(dt('Can not read file @file.', ['@file' => $file]));
        continue;
      }
      $poFile = (object) [
        'filename' => basename($file),
        'uri' => $file,
      ];
      $poFile = locale_translate_file_attach_properties($poFile, $translationOptions);
      if ($poFile->langcode == LanguageInterface::LANGCODE_NOT_SPECIFIED) {
        $this->logger()->warning(dt('Can not autodetect language of file @file. Supported filename patterns are: {project}-{version}.{langcode}.po, {prefix}.{langcode}.po or {langcode}.po.', [
          '@file' => $file,
        ]));
        continue;
      }
      if (!$this->getLanguageManager()->getLanguage($poFile->langcode)) {
        $this->logger()->warning(dt('Language @language does not exist for file @file', [
          '@language' => $poFile->langcode,
          '@file' => $file,
        ]));
        continue;
      }
      // Import translation file if language exists.
      $langcodes_to_import[$poFile->langcode] = $poFile->langcode;
      $files[$poFile->uri] = $poFile;
    }

    // Set a batch to download and import translations.
    $batch = locale_translate_batch_build($files, $translationOptions);
    batch_set($batch);
    if ($batch = locale_config_batch_update_components($translationOptions, $langcodes_to_import)) {
      batch_set($batch);
    }

    return drush_backend_batch_process();
  }

  private function convertCustomizedType($type): int
  {
    return $type == 'customized' ? LOCALE_CUSTOMIZED : LOCALE_NOT_CUSTOMIZED;
  }

  /**
   * Converts input of override option.
   *
   * @param $override
   */
  private function convertOverrideOption($override): array
  {
    $result = [];

    switch ($override) {
      case 'none':
        $result = [
          'not_customized' => false,
          'customized' => false,
        ];
        break;

      case 'customized':
        $result = [
          'not_customized' => false,
          'customized' => true,
        ];
        break;

      case 'not-customized':
        $result = [
          'not_customized' => true,
          'customized' => false,
        ];
        break;

      case 'all':
        $result = [
          'not_customized' => true,
          'customized' => true,
        ];
        break;
    }

    return $result;
  }

}

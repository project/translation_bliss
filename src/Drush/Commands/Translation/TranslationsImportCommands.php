<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Translation;

use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\locale\StringStorageInterface;
use Drupal\translation_bliss\Drush\Commands\Translation\Utility\TranslationLids;
use Drupal\translation_bliss\Drush\CoreFix\StringStorageFix;
use Drupal\translation_bliss\Drush\DrushFix\LocaleCommandsFix;
use Drush\Attributes\Command;
use Drush\Attributes\Option;
use Drush\Commands\AutowireTrait;
use Drush\Commands\core\CacheRebuildCommands;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;

final class TranslationsImportCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use AutowireTrait;
  use SiteAliasManagerAwareTrait;

  const TranslationsDirInitCommand = 'bliss:translations-dir-init';
  const TranslationsImportCommand = 'bliss:translations-import';

  const NoCacheRebuildOption = 'no-cache-rebuild';

  protected function __construct(
    private readonly StringStorageInterface $localeStorage,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly LanguageManagerInterface $languageManager,
  ) {
    parent::__construct();
  }

  /**
   * Initialize the translations directory to be under git control.
   */
  #[Command(name: self::TranslationsDirInitCommand, aliases: ['tdi'])]
  public function translationsDirInitCommand(string $translationsDir = '../translations') {
    $localeSettings = $this->configFactory->getEditable('locale.settings');
    $localeSettings->set('translation.path', $translationsDir);
    $localeSettings->set('update_interval_days', 0);
    $localeSettings->save();
    $this->logger()->success(sprintf('Set translations directory to "%s".', $translationsDir));
    $this->logger()->success('Set automatic translation updates to off / manual.');
  }

  /**
   * Rebuild all translations: Delete / Import contrib / Import custom
   */
  #[Command(name: self::TranslationsImportCommand, aliases: ['tim'])]
  #[Option(name: self::NoCacheRebuildOption, description: 'Do not rebuild cache after import.')]
  public function translationsImportCommand(string $translationsDir = NULL, array $options = [self::NoCacheRebuildOption => FALSE]) {
    $translationsDir = $translationsDir
      ?? $this->configFactory->get('locale.settings')->get('translation.path');
    if (str_starts_with($translationsDir, 'sites/default/files')) {
      throw new \UnexpectedValueException('Translations dir is not under git control. Call `drush bliss:translations-init` and `drush locale:update` to fix.');
    }

    // Avoid to delete all translations, as that takes a lot of time that a site
    // deploying is down. Instead, log string IDs ("lid") of all imported
    // translations and delete all translations of strings not in that set, but
    // not the strings.
    // Use the LocaleCommandsFix fork of drush locale:import-all that returns
    // those string IDs.

    $this->logger()->notice("Import downloaded contrib translations start.");
    $resultOfContrib = LocaleCommandsFix::create($this->logger())
      ->importAll(
        directory: "$translationsDir",
        type: 'not-customized',
        override: 'not-customized',
      );
    $this->logger()->success('Import contrib translations success');

    $this->logger()->notice('Import custom translations start');
    $resultOfCustom = LocaleCommandsFix::create($this->logger())
      ->importAll(
        directory: "$translationsDir/custom",
        type: 'customized',
        override: 'all',
      );
    $this->logger()->success('Import custom translations success');

    $this->logger()->notice('Deleting not-imported translations start');
    $translationLids = TranslationLids::fromBatchImportResults($resultOfContrib, $resultOfCustom);

    // Finally delete all not imported translations.
    // The ::deleteTranslations has no negated id filter, so query all first.
    foreach ($this->languageManager->getLanguages() as $language) {
      $langcode = $language->getId();

      // Get lids of all non-null translations.
      $languageTranslations = $this->localeStorage->getTranslations(['language' => $langcode]);
      $languageTranslationLids = [];
      foreach ($this->localeStorage->getTranslations(['language' => $langcode]) as $translation) {
        if ($translation->getString()) {
          $languageTranslationLids[] = $translation->getId();
        }
      }

      $languageImportedLids = $translationLids->forLangcode($langcode);
      $lidsToDelete = array_diff($languageTranslationLids, $languageImportedLids);

      if ($lidsToDelete) {
        StringStorageFix::create()->deleteTranslations(['lid' => $lidsToDelete, 'language' => $langcode]);
      }
      $this->logger()->notice(sprintf('Deleting %s not-imported %s translations', count($lidsToDelete), strtoupper($langcode)));
    }
    $this->logger()->success('Deleting not-imported translations success');

    if (!$options[self::NoCacheRebuildOption]) {
      /** @see \Drush\Commands\core\DeployCommands::cacheRebuild */
      $self = $this->siteAliasManager()->getSelf();
      $manager = $this->processManager();
      $redispatchOptions = Drush::redispatchOptions();

      $this->logger()->success("Cache rebuild start.");
      $process = $manager->drush($self, CacheRebuildCommands::REBUILD, [], $redispatchOptions);
      $process->mustRun($process->showRealtime());
    }
  }

}

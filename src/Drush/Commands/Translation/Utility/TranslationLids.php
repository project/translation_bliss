<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Translation\Utility;

/**
 * Structured storage of translation string IDs.
 */
final class TranslationLids {

  /**
   * @param array<string, list<int>> $lidsByLangcode
   */
  public function __construct(
    private readonly array $lidsByLangcode,
  ) {}

  public static function fromBatchImportResults(array ...$results): self {
    /** @see \Drupal\locale\PoDatabaseWriter::importString */
    /** @see \locale_translate_batch_import */
    $lidsByLangcode = [];
    foreach ($results as $result) {
      foreach ($result as $iterationIndex => $iterationResult) {
        if (!is_array($iterationResult)) {
          // "drush_batch_process_finished" => true
          continue;
        }
        foreach ($iterationResult['stats'] ?? [] as $fileUri => $fileResults) {
          if (!is_array($fileResults)) {
            // "config" => 0
            continue;
          }
          $langcode = self::extractLangcodeFromFileUri($fileUri);
          foreach ($fileResults['strings'] ?? [] as $lid) {
            assert(is_numeric($lid));
            assert(!str_contains($lid, '.'));
            $lid = intval($lid);
            $lidsByLangcode[$langcode][$lid] = $lid;
          }
        }
      }
    }
    $lidsByLangcode = array_map('array_values', $lidsByLangcode);
    return new self($lidsByLangcode);
  }

  private static function extractLangcodeFromFileUri(string $fileUri): string {
    /** @see \Drush\Commands\core\LocaleCommands::importAll */
    $poFile = (object) [
      'filename' => basename($fileUri),
      'uri' => $fileUri,
    ];
    $poFile = locale_translate_file_attach_properties($poFile);
    return $poFile->langcode;
  }

  /**
   * @return array list<int>
   */
  public function forLangcode(string $langcode): array {
    return $this->lidsByLangcode[$langcode] ?? [];
  }

}

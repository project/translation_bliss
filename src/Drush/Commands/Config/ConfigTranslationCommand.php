<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Config;

use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;
use Drupal\translation_bliss_config\ConfigOverride\TranslationBlissConfigTranslator;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

final class ConfigTranslationCommand extends DrushCommands {

  use AutowireTrait;

  const ConfigTranslationsCleanCommand = 'bliss:config-translations-clean';

  protected function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly LanguageManagerInterface $languageManager,
    protected readonly ?TranslationBlissConfigTranslator $blissConfigTranslator,
    protected readonly ?LanguageConfigFactoryOverrideInterface $languageConfigFactoryOverride,
  ) {
    parent::__construct();
  }

  #[CLI\Command(name: self::ConfigTranslationsCleanCommand, aliases: ['ctclean'])]
  public function configTranslationsCleanCommand() {
    // Ensure that translation_bliss_config is installed.
    if (!$this->blissConfigTranslator) {
      throw new \Exception('Please install the translation_bliss_config module required for this command.');
    }
    // Ensure that language is installed.
    if (!$this->languageConfigFactoryOverride) {
      throw new \Exception('Please install the language module required for this command.');
    }

    $allConfigNames = $this->configFactory->listAll();
    // Load all config into static cache.
    $allConfig = $this->configFactory->loadMultiple($allConfigNames);

    foreach ($this->languageManager->getLanguages() as $language) {
      foreach ($allConfig as $name => $config) {
        /** @var \Drupal\language\Config\LanguageConfigOverride $override */
        $override = $this->languageConfigFactoryOverride->getOverride($language->getId(), $name);
        $blissTranslation = $this->blissConfigTranslator->getTranslatableConfig($name, $language->getId());
        $requiredOverrideData = DiffArray::diffAssocRecursive($override->getRawData(), $blissTranslation);
        if (!$requiredOverrideData) {
          $override->delete();
        }
        else {
          $diffCurrentToRequired = DiffArray::diffAssocRecursive($override->getRawData(), $requiredOverrideData);
          if ($diffCurrentToRequired) {
            $override->setData($requiredOverrideData)->save();
          }
        }
      }
    }
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Config;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

final class ConfigCommands extends DrushCommands {

  use AutowireTrait;

  const ConfigLanguageSummaryCommand = 'bliss:config-language-summary';

  const ConfigLanguageDetailsCommand = 'bliss:config-language-details';

  const ConfigLanguageConvertCommand = 'bliss:config-language-convert';

  protected function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly LanguageManagerInterface $languageManager,
  ) {
    parent::__construct();
  }

  /**
   * Provide summary counts of config and their languages.
   */
  #[CLI\Command(name: self::ConfigLanguageSummaryCommand, aliases: ['clangs'])]
  #[CLI\FieldLabels(labels: ['language' => 'Language', 'count' => 'Count'])]
  #[CLI\DefaultTableFields(fields: ['language', 'count'])]
  #[CLI\FilterDefaultField(field: 'language')]
  public function summary(): ?RowsOfFields {
    $rows = [];
    foreach ($this->getConfigNamesByLangcode() as $langcode => $names) {
      $rows[] = [
        'language' => $langcode,
        'count' => count($names),
      ];
    }

    return new RowsOfFields($rows);
  }

  /**
   * Provide details of config and their languages.
   */
  #[CLI\Command(name: self::ConfigLanguageDetailsCommand, aliases: ['clangd'])]
  #[CLI\FieldLabels(labels: ['language' => 'Language', 'name' => 'Name'])]
  #[CLI\DefaultTableFields(fields: ['language', 'name'])]
  #[CLI\FilterDefaultField(field: 'name')]
  public function details(): ?RowsOfFields {
    $rows = [];
    foreach ($this->getConfigNamesByLangcode() as $langcode => $names) {
      foreach ($names as $name) {
        $rows[] = [
          'language' => $langcode,
          'name' => $name,
        ];
      }
    }

    return new RowsOfFields($rows);
  }

  public function getConfigNamesByLangcode(): array {
    $allConfigNames = $this->configFactory->listAll();
    // Load all config into static cache.
    $this->configFactory->loadMultiple($allConfigNames);

    $byLangcode = [];
    $byLangcode['en'] = [];
    foreach ($allConfigNames as $name) {
      // Get config override free.
      $config = $this->configFactory->getEditable($name);
      if ($langcode = $config->get('langcode')) {
        $byLangcode[$langcode][] = $name;
      }
    }
    return $byLangcode;
  }

  /**
   * Bulk convert config language (but does not translate strings).
   */
  #[CLI\Command(name: self::ConfigLanguageConvertCommand, aliases: ['clangc'])]
  #[CLI\Argument(name: 'targetLanguage', description: 'The language code to convert to.')]
  #[CLI\Argument(name: 'configNames', description: 'A list of config names to convert.')]
  #[CLI\Option(name: 'all', description: 'Convert all config to language.')]
  #[CLI\FieldLabels(labels: ['originalLanguage' => 'Original language', 'targetLanguage' => 'Target language', 'name' => 'Name'])]
  #[CLI\DefaultTableFields(fields: ['originalLanguage', 'targetLanguage', 'name'])]
  public function convert(
    string $targetLanguage,
    array $configNames,
    array $options = [
      'all' => FALSE,
    ],
  ): ?RowsOfFields {
    $convertAll = $options['all'];
    if ($convertAll && $configNames) {
      throw new \Exception('Can not combine configNames and --all.');
    }
    if (!$this->languageManager->getLanguage($targetLanguage)) {
      throw new \Exception("Unknown language: $targetLanguage");
    }

    $allNames = [];
    foreach ($this->getConfigNamesByLangcode() as $currentLangcode => $names) {
      if ($currentLangcode === $targetLanguage) {
        continue;
      }
      foreach ($names as $name) {
        if ($convertAll || in_array($name, $configNames)) {
          $config = $this->configFactory->getEditable($name);
          $config->set('langcode', $targetLanguage);
          $config->save();

          $allNames[$currentLangcode][$targetLanguage][] = $name;
        }
      }
    }

    $rows = [];
    foreach ($allNames as $originalLangcode => $targetNames) {
      foreach ($targetNames as $targetLangcode => $names) {
        foreach ($names as $name) {
          $rows[] = [
            'originalLanguage' => $originalLangcode,
            'targetLanguage' => $targetLangcode,
            'name' => $name,
          ];
        }
      }
    }

    if (!$rows) {
      $this->output()->writeln('No translation language converted. Use --all if you want that.');
    }

    return new RowsOfFields($rows);
  }

}

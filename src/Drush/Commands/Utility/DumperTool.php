<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Utility;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @internal
 */
final class DumperTool {

  public static function dumpToHtmlFile(string $fileName = 'dump.html', int $maxDepth = 1) {
    VarDumper::setHandler(function ($v) use ($fileName, $maxDepth) {
      $cloner = new VarCloner();
      $dumper = new HtmlDumper();
      $dumper->setDisplayOptions(['maxDepth' => $maxDepth]);
      $output = fopen($fileName, 'a');
      $dumper->dump($cloner->cloneVar($v), $output);
    });
  }

  public static function dumpToTxtFile(string $fileName = 'dump.txt', int $maxDepth = 2) {
    VarDumper::setHandler(function ($v) use ($fileName, $maxDepth) {
      $cloner = new VarCloner();
      $dumper = new CliDumper();
      $dumper->setDisplayOptions(['maxDepth' => $maxDepth]);
      $output = fopen($fileName, 'a');
      $dumper->dump($cloner->cloneVar($v), $output);
    });
  }

}

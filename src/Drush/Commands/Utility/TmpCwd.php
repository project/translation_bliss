<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Utility;

/**
 * Change CurrentWOrkingDirectory temorarily.
 *
 * @internal
 */
final class TmpCwd {

  private function __construct(
    private readonly string $currentDir,
  ) {}

  public static function create(
    string $targetDir,
  ): ?self {
    $currentDir = getcwd();
    $success = chdir($targetDir);
    return $success ? new self($currentDir) : NULL;
  }

  public function __destruct() {
    chdir($this->currentDir);
  }

}

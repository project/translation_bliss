<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\locale\LocaleDefaultConfigStorage;
use Drupal\translation_bliss\ConfigExclude\TranslationBlissConfigExcludeInterface;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer\SourceItemWriterInterface;
use Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHooksBase;
use Drupal\translation_bliss\SourceLanguage\SrcLang;
use Drush\Attributes\Hook;
use Drush\Attributes\Option;
use Drush\Commands\AutowireTrait;

/**
 * @internal
 */
final class ConfigTranslationExtractionHookCommands extends TranslationExtractionHooksBase {

  use AutowireTrait;

  const DoNotExtractOption = 'no-config';

  protected bool $doNotExtract;

  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly TypedConfigManagerInterface $typedConfigManager,
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly TranslationBlissConfigExcludeInterface $configExclude,
    private readonly LocaleDefaultConfigStorage $localeDefaultConfigStorage,
  ) {
    parent::__construct();
  }

  private function extractConfig(SourceItemWriterInterface $writer): void {
    // Parsing config via _potx_process_module_schemas works out badly, as
    // there is no simple way to determine the providing module and load its
    // schema.
    // So load runtime config, but use config/sync directory to hint file.
    $configNames = $this->configFactory->listAll();
    // Prime the static cache.
    $this->configFactory->loadMultiple($configNames);
    foreach ($configNames as $configName) {
      $config = $this->configFactory->getEditable($configName);
      $typedConfig = $this->typedConfigManager->createFromNameAndData($configName, $config->getRawData());
      $this->output()->writeln("Processing config $configName");
      $this->processTypedConfig($typedConfig, $config->get('langcode'), $writer);
    }
  }

  private function processTypedConfig(TypedDataInterface $element, ?string $langcode, SourceItemWriterInterface $writer): void {
    // Ignore excluded config like webform elements.
    if ($this->configExclude->isConfigTranslationExcluded($element)) {
      $this->output()->writeln("Skipping excluded config property {$element->getPropertyPath()}");
      return;
    }
    // Ignore shipped config.
    if ($element->getValue() === $this->getLocaleDefaultData($element)) {
      return;
    }

    /** Adapted from @see \Drupal\locale\LocaleConfigManager::getTranslatableData */
    if ($element instanceof TraversableTypedDataInterface) {
      foreach ($element as $property) {
        $this->processTypedConfig($property, $langcode, $writer);
      }
    }
    else {
      $value = $element->getValue();
      $definition = $element->getDataDefinition();
      if (!empty($definition['translatable']) && $value !== '' && $value !== NULL) {
        $context = $definition['translation context'] ?? NULL;
        if ($langcode && $langcode !== 'en' && $langcode !== LanguageInterface::LANGCODE_SYSTEM) {
          $context = SrcLang::create($langcode)->context($context ?? '');
        }
        [$fileName, $occurrence] = $this->getConfigFileAndOccurrence($element);
        $writer->writeSourceItem($value, NULL, $context, $fileName, $occurrence);
      }
    }
  }

  private function getConfigFileAndOccurrence(TypedDataInterface $element): array {
    $configName = $element->getRoot()->getPropertyPath();
    $propertyPath = $element->getPropertyPath();
    $configPath = substr($propertyPath, strlen($configName) + 1);
    return ["Config:$configName", $configPath];
  }

  private function getLocaleDefaultData(TypedDataInterface $element): mixed {
    $configName = $element->getRoot()->getPropertyPath();
    $propertyPath = $element->getPropertyPath();
    $configPath = substr($propertyPath, strlen($configName) + 1);
    $localeDefaultConfig = $this->localeDefaultConfigStorage->read($configName);
    if ($configPath && isset($localeDefaultConfig)) {
      assert(is_array($localeDefaultConfig));
      $parents = explode('.', $configPath);
      $result = NestedArray::getValue($localeDefaultConfig, $parents);
    }
    else {
      $result = $localeDefaultConfig;
    }

    return $result;
  }


  #[Hook(type: HookManager::OPTION_HOOK, target: TranslationExtractionCommands::TranslationsExtractCommand)]
  #[Option(name: self::DoNotExtractOption, description: 'Do not extract config/sync values.')]
  public function optionsPotxX($options = [self::DoNotExtractOption => FALSE]): void {}

  #[Hook(type: HookManager::ARGUMENT_VALIDATOR, target: TranslationExtractionCommands::TranslationsExtractCommand)]
  public function readOptions(CommandData $commandData): void {
    $this->doNotExtract = $commandData->input()->getOption(self::DoNotExtractOption) ?? FALSE;
  }

  protected function hookFinishProcessing(SourceItemWriterInterface $writer): void {
    if ($this->doNotExtract) {
      $this->output()->writeln('Skipping config extraction.');
      return;
    }
    $this->output()->writeln('Extracting config.');
    $this->extractConfig($writer);
  }

  protected function hookStartProcessing(array $sourceDirs): void {
    // Nothing to do. Just to show off and test the API:
  }

  protected function hookAlterCollectedFiles(array &$files, array $sourceDirs): void {
    // Nothing to do. Just to show off and test the API:
  }

  protected function hookProcessFiles(array $files, SourceItemWriterInterface $writer): void {
    // Nothing to do. Just to show off and test the API:
  }

}

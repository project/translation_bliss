<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi;

/**
 * @api
 */
final class TranslationExtractionHook {

  /**
   * @see \Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHooksBase::hookStartProcessing
   * @api
   */
  public const StartProcessing = 'translation_bliss_extraction_start_processing';

  /**
   * @see \Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHooksBase::hookAlterCollectedFiles
   * @api
   */
  public const AlterCollectedFiles = 'translation_bliss_extraction_alter_collected_files';

  /**
   * @see \Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHooksBase::hookProcessFiles
   * @api
   */
  public const ProcessFiles = 'translation_bliss_extraction_process_files';

  /**
   * @see \Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHooksBase::hookFinishProcessing
   * @api
   */
  public const FinishProcessing = 'translation_bliss_extraction_finish_processing';

}

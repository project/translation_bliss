<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi;

use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer\SourceItemWriterInterface;
use Drush\Attributes\Hook;
use Drush\Commands\DrushCommands;

/**
 * A convenience base class to implement the api.
 *
 * You are subclassing this to implement an extension? Tell us!
 * https://www.drupal.org/node/add/project-issue/translation_bliss
 *
 * @todo Upstream: Drush does not recognize hook implementation in trait.
 *
 * @api
 */
abstract class TranslationExtractionHooksBase extends DrushCommands {

  /**
   * @param list<string> $sourceDirs
   */
  #[Hook(type: HookManager::ON_EVENT, target: TranslationExtractionHook::StartProcessing)]
  public function wrapHookStartProcessing(array $sourceDirs): void {
    $this->hookStartProcessing($sourceDirs);
  }

  /**
   * @param list<string> &$files
   * @param list<string> $sourceDirs
   *
   * @return list<string>
   *   A list of files to process.
   */
  #[Hook(type: HookManager::ON_EVENT, target: TranslationExtractionHook::AlterCollectedFiles)]
  public function wrapHookAlterCollectedFiles(array &$files, array $sourceDirs): void {
    $this->hookAlterCollectedFiles($files, $sourceDirs);
  }

  /**
   * @param list<string> $files
   */
  #[Hook(type: HookManager::ON_EVENT, target: TranslationExtractionHook::ProcessFiles)]
  public function wrapHookProcessFiles(array $files, SourceItemWriterInterface $writer): void {
    $this->hookProcessFiles($files, $writer);
  }

  #[Hook(type: HookManager::ON_EVENT, target: TranslationExtractionHook::FinishProcessing)]
  public function wrapHookFinishProcessing(SourceItemWriterInterface $writer): void {
    $this->hookFinishProcessing($writer);
  }

  /**
   * @param list<string> $sourceDirs
   */
  abstract protected function hookStartProcessing(array $sourceDirs): void;

  /**
   * @param list<string> &$files
   * @param list<string> $sourceDirs
   *
   * @return list<string>
   *   A list of files to process.
   */
  abstract protected function hookAlterCollectedFiles(array &$files, array $sourceDirs): void;

  /**
   * @param list<string> $files
   */
  abstract protected function hookProcessFiles(array $files, SourceItemWriterInterface $writer): void;

  abstract protected function hookFinishProcessing(SourceItemWriterInterface $writer): void;

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Utility;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage\SourceItemMemoryStorage;

/**
 * @internal
 */
final class PotxResult {

  public function __construct(
    public readonly array $files,
    public readonly SourceItemMemoryStorage $sourceItemStorage,
    public readonly array $errors,
  ) {}

}

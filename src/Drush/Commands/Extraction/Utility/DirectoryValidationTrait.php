<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Utility;

use Drupal\Core\File\FileSystemInterface;

/**
 * @internal
 */
trait DirectoryValidationTrait {

  /**
   * @param list<string> $dirs
   */
  private function filterDirs(array &$dirs): void {
    foreach ($dirs as $index => $dir) {
      if (!is_dir($dir)) {
        unset($dirs[$index]);
      }
    }
  }

  private function ensureDir(string &$dir): void {
    $dir = rtrim($dir, '/');
    $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)
      || throw new \UnexpectedValueException(sprintf("Not a directory: '%s', cwd='%s'", $dir, getcwd()));
  }

}

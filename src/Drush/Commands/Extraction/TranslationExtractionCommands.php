<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\locale\StringStorageInterface;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\LegacySaveStringAdapter;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage\SourceItemMemoryStorage;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer\SourceItemMemoryWriter;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Reader\TranslationItemStorageReader;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Storage\TranslationItemMemoryStorage;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Writer\TranslationItemStreamWriter;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\TranslationBlissPoHeader;
use Drupal\translation_bliss\Drush\Commands\Extraction\HooksApi\TranslationExtractionHook;
use Drupal\translation_bliss\Drush\Commands\Extraction\Utility\DirectoryValidationTrait;
use Drupal\translation_bliss\Drush\Commands\Extraction\Utility\PotxResult;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

/**
 * Class PotxXCommands.
 *
 * Potx commands for site builders.
 *
 * @internal
 */
class TranslationExtractionCommands extends DrushCommands implements CustomEventAwareInterface {

  use CustomEventAwareTrait;
  use AutowireTrait;
  use DirectoryValidationTrait;

  public const TranslationsExtractCommand = 'bliss:translations-extract';

  const DefaultSourceDirs = 'modules/custom,themes/custom';
  const DefaultTargetDir = '../translations/custom';

  const FileCustomStringsAllWithOrigins = 'custom-strings.all-with-origins.pot';
  const FileCustomStringsUntranslatable = 'adjustments/custom-strings.untranslatable.pot';
  const FileCustomStringsTranslatable = 'custom-strings.translatable.pot';
  const FileCustomStringsTranslations = 'custom-strings.%s.po';
  const FileContribTranslatedCommonStrings = 'contrib-translated-common-strings.%s._po';
  const FileCustomTranslatedContribStrings = 'custom-translated-contrib-strings.%s.po';

  public function __construct(
    protected readonly ModuleHandlerInterface $moduleHandler,
    protected readonly LanguageManagerInterface $languageManager,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly StringStorageInterface $stringStorage,
  ) {
    parent::__construct();
  }

  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: TranslationExtractionCommands::TranslationsExtractCommand)]
  public function validatePotxInstalled(CommandData $commandData): void {
    // Ensure that potx is installed. Potx depends on locale.
    if (!$this->moduleHandler->moduleExists('potx')) {
      throw new \Exception('Please install the potx module required for this command.');
    }
  }

  /**
   * Extract translatable strings from custom code and all config, optimized for site owners.
   */
  #[CLI\Command(name: self::TranslationsExtractCommand, aliases: ['tx', 'tex', 'potxx'])]
  #[CLI\Usage(name: 'tex', description: 'Translation template extraction optimized for site owners.')]
  #[CLI\Option(name: 'source', description: 'One or more source folders.')]
  #[CLI\Option(name: 'target', description: 'The target folder.')]
  #[CLI\FieldLabels(labels: [
    'files' => 'Files',
    'strings' => 'Strings',
    'errors' => 'Errors',
  ])]
  public function extractAllFilesCommand(array $options = [
    'source' => self::DefaultSourceDirs,
    'target' => self::DefaultTargetDir,
  ]): RowsOfFields {
    $sourceDirs = explode(',', $options['source']);
    $this->filterDirs($sourceDirs);

    $targetDir = $options['target'];
    $this->ensureDir($targetDir);
    $adjustmentsDir = "$targetDir/adjustments";
    $this->ensureDir($adjustmentsDir);

    // Extract.
    $result = $this->extract($sourceDirs);
    $customStringsWithComments = $result->sourceItemStorage;
    $rows[] = [
      'files' => count($result->files),
      'strings' => count($customStringsWithComments->getItems()),
      'errors' => count($result->errors),
    ];

    // Read custom-strings.untranslatable.pot.
    $untranslatableStringsFileName = "$targetDir/" . self::FileCustomStringsUntranslatable;
    if (is_file($untranslatableStringsFileName) ) {
      $untranslatableTranslations = TranslationItemMemoryStorage::fromPoFile($untranslatableStringsFileName);
      $untranslatableStrings = $untranslatableTranslations->sources();
    }
    else {
      $untranslatableStrings = new SourceItemMemoryStorage();
    }

    // From now on, drop untranslatable custom strings.
    $translatableStrings = $customStringsWithComments
      ->without($untranslatableStrings)
      ->withoutComments();

    // Write custom-strings.all-with-origin.pot
    $this->writeTranslations(
      translations: TranslationItemMemoryStorage::fromSourceCollection($customStringsWithComments),
      langcode: '',
      fileName: "$targetDir/" . self::FileCustomStringsAllWithOrigins,
      projectName: '', // Is ignored for template.
      generatedFrom: $result->files
    );

    // Add the source comments and write it back as ordered po template.
    $this->writeTranslations(
      translations: TranslationItemMemoryStorage::fromSourceCollection($untranslatableStrings),
      langcode: '',
      fileName: $untranslatableStringsFileName,
      projectName: '', // Is ignored for template.
    );

    // Write custom-strings.translatable.pot
    $this->writeTranslations(
      translations: TranslationItemMemoryStorage::fromSourceCollection($translatableStrings),
      langcode: '',
      fileName: "$targetDir/" . self::FileCustomStringsTranslatable,
      projectName: '', // Is ignored for template.
    );

    foreach ($this->languageManager->getLanguages() as $language) {
      if ($language->getId() === 'en') {
        if (!$this->configFactory->get('locale.settings')->get('translate_english')) {
          continue;
        }
      }

      $customSources = SourceItemMemoryStorage::copyWithoutComments($translatableStrings);

      // @todo Consider retrieving only the translations of the sources.
      $translations = TranslationItemMemoryStorage::fromCoreStringStorage($language->getId(), $this->stringStorage);
      $emptyTranslations = $translations->onlyEmpty();
      $customTranslations = $translations->onlyCustomized();
      $contribTranslations = $translations->onlyNotCustomized();

      // Write custom-strings.XX.po
      // - Drop custom sources with contrib translations.
      //   Those go to contrib-translated-common-strings.
      // - Then add translations if they exist.
      $trueCustomSources = $customSources
        ->without($contribTranslations->sources());
      $customTranslationsOfTrueCustomSources = $customTranslations
        ->filterHavingSourceStrings($trueCustomSources);
      $customOrEmptyTranslationsOfTrueCustomSources = $customTranslationsOfTrueCustomSources
        ->withFallback(TranslationItemMemoryStorage::fromSourceCollection($trueCustomSources));
      $this->writeTranslations(
        translations: $customOrEmptyTranslationsOfTrueCustomSources,
        langcode: $language->getId(),
        fileName: "$targetDir/" . sprintf(self::FileCustomStringsTranslations, $language->getId()),
        projectName: 'custom-code-and-config strings that have a custom or no translation.',
      );

      // Write contrib-translated-common-strings.XX._po
      // Note that common-strings has no .po extension, to not have it imported.
      // All its translations already come with module translation files.
      //
      // Also note that, for perfect transparency, some of the custom-strings
      // items should go to a custom-translated-common-strings.XX.po and
      // untranslated-common-strings.XX.po.
      // @todo Someday implement custom-translated-common-strings.XX.po
      //   and untranslated-common-strings.XX.po.
      //   Note that one obstacle to this is the translation server only providing
      //   translated strings, with no option for all strings.
      $contribTranslationsOfCustomSources = $contribTranslations
        ->filterHavingSourceStrings($customSources);
      $this->writeTranslations(
        translations: $contribTranslationsOfCustomSources,
        langcode: $language->getId(),
        fileName: "$targetDir/" . sprintf(self::FileContribTranslatedCommonStrings, $language->getId()),
        projectName: 'custom-code-and-config strings that already have a translation from contrib.',
      );

      // Write custom-translated-contrib-strings.XX.po
      $customTranslationsNotFromCustomStrings = $customTranslations
        ->filterNotHavingSourceStrings($customSources);
      $this->writeTranslations(
        translations: $customTranslationsNotFromCustomStrings,
        langcode: $language->getId(),
        fileName: "$targetDir/" . sprintf(self::FileCustomTranslatedContribStrings, $language->getId()),
        projectName: 'contrib strings or vanished custom strings that have a custom translation.',
      );
    }

    // Print results.
    if (!empty($result->errors)) {
      $this->output()->writeln('');
      $this->output()->writeln(dt('Translation extraction warnings:'));
      foreach (array_unique($result->errors) as $error) {
        $this->logger()->error($error);
      }
      $this->output()->writeln('');
    }
    $this->output()->writeln('');
    return new RowsOfFields($rows);
  }

  protected function extract(
    array $sourceDirs,
  ): PotxResult {
    // Include library.
    $this->moduleHandler->loadInclude('potx', 'inc');
    $this->moduleHandler->loadInclude('potx', 'inc', 'potx.local');

    $files = [];

    // Silence error message reporting. Messages will be reported by at the end.
    potx_status('set', POTX_STATUS_SILENT);

    foreach ($sourceDirs as $sourceDir) {
      potx_local_init(
        module_path: $sourceDir,
      );
    }
    foreach ($this->getCustomEventHandlers(TranslationExtractionHook::StartProcessing) as $hookStartProcessing) {
      $hookStartProcessing($sourceDirs);
    }

    foreach ($sourceDirs as $sourceDir) {
      $files = array_merge(
        $files,
        _potx_explore_dir(
          path: $sourceDir,
          skip_self: TRUE
        )
      );
    }
    foreach ($this->getCustomEventHandlers(TranslationExtractionHook::AlterCollectedFiles) as $hookAlterCollectedFiles) {
      $hookAlterCollectedFiles($files, $sourceDirs);
    }

    // Write to our own storage.
    $writer = new SourceItemMemoryWriter();
    $saveCallback = new LegacySaveStringAdapter($writer);

    foreach ($files as $file) {
      $this->output()->writeln("Processing file $file...");
      _potx_process_file(
        file_path: $file,
        save_callback: $saveCallback,
      );
    }
    foreach ($this->getCustomEventHandlers(TranslationExtractionHook::ProcessFiles) as $hookProcessFiles) {
      $hookProcessFiles($files, $writer);
    }

    // Skip potx_finish_processing.
    // All of the steps executed there are not needed here:
    // - store_module_metadata - not needed.
    // - _potx_parse_shipped_configuration - this is parsing in-memory config
    //   instead to cover all config.
    // - clear $_potx_yaml_translation_patterns for the next module - not needed
    //   here as there is no next module.

    // @todo Allow extensions to
    //   - add processed files / objects
    //   - log errors
    foreach ($this->getCustomEventHandlers(TranslationExtractionHook::FinishProcessing) as $hookFinishProcessing) {
      $hookFinishProcessing($writer);
    }

    // Get errors, if any.
    $errors = potx_status('get', TRUE);

    return new PotxResult(
      files: $files,
      sourceItemStorage: $writer->freeze(),
      errors: $errors,
    );
  }

  // @todo Move method to TranslationItemStreamWriter.
  private function writeTranslations(TranslationItemMemoryStorage $translations, string $langcode, string $fileName, string $projectName, array $generatedFrom = []): void {
    $header = new TranslationBlissPoHeader($langcode);
    $header->setLanguageName(strtoupper($langcode));
    $header->setProjectName($projectName);

    $translationReader = TranslationItemStorageReader::fromStorage($translations);
    $translationsWriter = new TranslationItemStreamWriter($fileName, $header, $generatedFrom);
    $translationsWriter->writeFromReader($translationReader);
    unset($translationsWriter); // Explicitly close stream.
  }

}

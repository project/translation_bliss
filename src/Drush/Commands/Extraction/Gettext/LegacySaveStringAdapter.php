<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext;

use Drupal\Component\Gettext\PoItem;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer\SourceItemWriterInterface;

/**
 * @internal
 */
final class LegacySaveStringAdapter {

  /**
   * The \00 plural separator of incoming source strings.
   *
   * Note that this is different from the \03 plural delimiter used in many
   * other places like database, PoMemoryWriter etc.
   *
   * @see \_potx_find_format_plural_calls
   * @see \Drupal\Component\Gettext\PoItem::DELIMITER
   */
  private const PoTranslationsExtractorPluralDelimiter = "\0";

  public function __construct(
    private readonly SourceItemWriterInterface $writer,
  ) {}

  public function __invoke(...$arguments) {
    return $this->_potx_save_string(...$arguments);
  }

  /**
   * @see \_potx_save_string
   */
  private function _potx_save_string(
    ?string $value = NULL,
    ?string $context = NULL,
    ?string $file = NULL,
    int $line = 0,
    int $string_mode = POTX_STRING_RUNTIME
  ): ?array {
    if (isset($value)) {
      if (str_contains($value, self::PoTranslationsExtractorPluralDelimiter)) {
        [$source, $plural] = explode(self::PoTranslationsExtractorPluralDelimiter, $value, 2);
      }
      else {
        $source = $value;
        $plural = NULL;
      }

      $this->writer->writeSourceItem($source, $plural, $context, "File:$file", $line);
    }
    else {
      throw new \InvalidArgumentException('Retrieving values is not supported');
    }
    return NULL;
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Trait\SourceItemStorageKeyTrait;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem;

/**
 * @internal
 */
final class SourceItem {

  use SourceItemStorageKeyTrait;

  public function __construct(
    public readonly string $source,
    public readonly ?string $pluralSource,
    public readonly string $context,
    public readonly string $comment = '',
  ) {}

  public static function fromTranslation(TranslationItem $translation): self {
    return new self(
      source: $translation->source,
      pluralSource: $translation->pluralSource,
      context: $translation->context,
      comment: $translation->comment,
    );
  }

  public function withoutComment(): self {
    return new self(
      source: $this->source,
      pluralSource: $this->pluralSource,
      context: $this->context,
      comment: '',
    );
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Trait\SourceItemStorageKeyTrait;

/**
 * @internal
 */
final class SourceItemBuilder {

  use SourceItemStorageKeyTrait;

  /**
   * @var array<string, list<int|string>>
   */
  private array $occurrences = [];

  public function __construct(
    private readonly string $source,
    private readonly ?string $plural,
    private readonly string $context,
  ) {}

  public function addOccurrence(string $file, int|string $fileLocation): void {
    $this->occurrences[$file][] = $fileLocation;
  }

  public function freeze(): SourceItem {
    return new SourceItem(
      source: $this->source,
      pluralSource: $this->plural,
      context: $this->context,
      comment: $this->formatOccurrenceComments(),
    );
  }

  private function formatOccurrenceComments(): string {
    /** @see \_potx_build_files */
    $occurrences = [];
    foreach ($this->occurrences as $file => $fileLocation) {
      // Prefixing # will be done later.
      /** @see \Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem::toString */
      $occurrences[] = ": $file:" . implode(';', $fileLocation);
    }
    return implode("\n", $occurrences);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer;

/**
 * @api Used by drush hooks.
 */
interface SourceItemWriterInterface {

  public function writeSourceItem(string $source, ?string $plural, ?string $context, string $file, int $lineOrLocation): void;

}

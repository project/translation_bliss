<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Writer;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItemBuilder;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage\SourceItemMemoryStorage;

/**
 * @internal
 */
final class SourceItemMemoryWriter implements SourceItemWriterInterface {

  /**
   * @var array<string, \Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItemBuilder>
   */
  private array $items = [];

  public function __construct() {}

  public function freeze(): SourceItemMemoryStorage {
    $items = array_map(
      fn(SourceItemBuilder $item) => $item->freeze(),
      $this->items,
    );
    return new SourceItemMemoryStorage(...array_values($items));
  }

  public function writeSourceItem(string $source, ?string $plural, ?string $context, string $file, int|string $lineOrLocation): void {
    $item = new SourceItemBuilder($source, $plural, $context ?? '');
    if ($existingItem = $this->items[$item->getStorageKey()] ?? NULL) {
      $item = $existingItem;
    }
    else {
      $this->items[$item->getStorageKey()] = $item;
    }
    $item->addOccurrence($file, $lineOrLocation);
  }

}

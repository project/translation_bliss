<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem;

/**
 * @internal
 */
interface SourceItemStorageInterface {

  /**
   * @return list<\Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem>
   */
  public function getItems(): array;

  public function getSameSourceItem(SourceItem $sourceItem): ?SourceItem;

}

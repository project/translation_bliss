<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem;

/**
 * @internal
 */
final class SourceItemMemoryStorage implements SourceItemStorageInterface {

  private readonly array $items;

  /**
   * @internal Only to be called from SourceItemMemoryWriter::freeze.
   */
  public function __construct(SourceItem ...$items) {
    $indexedItems = [];
    foreach ($items as $item) {
      $indexedItems[$item->getStorageKey()] = $item;
    }
    ksort($indexedItems, SORT_NATURAL | SORT_FLAG_CASE);
    $this->items = $indexedItems;
  }

  public static function copyWithoutComments(SourceItemStorageInterface $storage): self {
    $items = [];
    foreach ($storage->getItems() as $item) {
      $items[] = $item->withoutComment();
    }
    return new self(...$items);
  }

  public function getItems(): array {
    return array_values($this->items);
  }

  public function getSameSourceItem(SourceItem $sourceItem): ?SourceItem {
    return $this->items[$sourceItem->getStorageKey()] ?? NULL;
  }

  public function withoutComments(): self {
    $items = [];
    foreach ($this->getItems() as $item) {
      $items[] = $item->withoutComment();
    }
    return new self(...$items);
  }


  public function without(SourceItemStorageInterface $sources): self {
    $items = [];
    foreach ($this->items as $item) {
      if (!$sources->getSameSourceItem($item)) {
        $items[] = $item;
      }
    }
    return new self(...$items);
  }

}

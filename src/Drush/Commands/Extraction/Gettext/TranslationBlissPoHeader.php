<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext;

use Drupal\Component\Gettext\PoHeader;

final class TranslationBlissPoHeader extends PoHeader {

  public function __construct($langcode = NULL) {
    parent::__construct($langcode);

    // Override the dynamic date and set it to a constant string, so to not
    // make more git noise than needed.
    $this->poDate = 'DATE';
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Trait;

/**
 * @internal
 */
trait SourceItemStorageKeyTrait {

  public function getStorageKey(): string {
    return implode("\03", [$this->context, $this->source, $this->pluralSource ?? '']);
  }

}

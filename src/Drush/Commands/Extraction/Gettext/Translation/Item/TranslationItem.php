<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item;

use Drupal\Component\Gettext\PoItem;
use Drupal\locale\TranslationString;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Trait\SourceItemStorageKeyTrait;

/**
 * @internal
 */
final class TranslationItem {

  use SourceItemStorageKeyTrait;

  /**
   * @param list<string> $translationAndPlurals
   */
  private function __construct(
    public readonly string $source,
    public readonly ?string $pluralSource,
    public readonly string $context,
    public readonly string $langcode,
    public readonly array $translationAndPlurals,
    public readonly string $comment,
    public readonly ?bool $customized,
  ) {
    // Translation (if exists) is plural iff source is plural.
    if ($this->pluralSource) {
      assert(count($this->translationAndPlurals) !== 1);
    }
    else {
      assert(count($this->translationAndPlurals) < 2);
    }
  }

  /**
   * @param list<string> $translationAndPlurals
   */
  public static function fromSource(
    SourceItem $sourceItem,
    string $langcode,
    array $translationAndPlurals = [],
  ): self {
    $translationAndPlurals = array_filter($translationAndPlurals);
    return new self(
      source: $sourceItem->source,
      pluralSource: $sourceItem->pluralSource,
      context: $sourceItem->context,
      langcode: $langcode,
      translationAndPlurals: $translationAndPlurals,
      comment: $sourceItem->comment,
      customized: NULL,
    );
  }

  public static function fromCoreTranslationString(
    string $langcode,
    TranslationString $translationString,
  ): self {
    [$source, $pluralSource] = explode(PoItem::DELIMITER, $translationString->source, 2)
      + [1 => NULL];
    $translationAndPlurals = array_filter(explode(PoItem::DELIMITER, $translationString->translation ?? ''));
    return new self(
      source: $source,
      pluralSource: $pluralSource,
      context: $translationString->context,
      langcode: $langcode,
      translationAndPlurals: $translationAndPlurals,
      comment: '',
      /** @see LOCALE_CUSTOMIZED */
      customized: boolval($translationString->customized),
    );
  }

  public static function fromCoreItem(PoItem $poItem): self {
    [$source, $pluralSource] = (array) $poItem->getSource() + [1 => NULL];
    return new self(
      source: $source,
      pluralSource: $pluralSource,
      context: $poItem->getContext(),
      langcode: $poItem->getLangcode(),
      translationAndPlurals: (array) $poItem->getTranslation(),
      comment: '',
      customized: NULL,
    );
  }

  public function toString(): string {
    // Delegate to core PoItem for now.
    $coreItem = new PoItem();
    if ($this->pluralSource) {
      $coreItem->setPlural(TRUE);
      $coreItem->setSource([$this->source, $this->pluralSource]);
      $coreItem->setContext($this->context);
      $coreItem->setTranslation($this->translationAndPlurals);
    }
    else {
      $coreItem->setPlural(FALSE);
      $coreItem->setSource($this->source);
      $coreItem->setContext($this->context);
      $coreItem->setTranslation($this->translationAndPlurals[0] ?? '');
    }

    $return = strval($coreItem);

    if ($this->comment) {
      // Comment may be multiline.
      $commentLines = explode("\n", $this->comment);
      $commentLines = array_map(
        fn(string $line) => "#$line",
        $commentLines,
      );
      $return = sprintf("%s\n%s", implode("\n", $commentLines), $return);
    }
    return $return;
  }

  public function hasEmptyTranslation(): bool {
    return empty($this->translationAndPlurals);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Reader;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Storage\TranslationItemStorageInterface;

/**
 * @internal
 */
final class TranslationItemStorageReader implements TranslationItemReaderInterface {

  private int $currentItemKey = 0;

  /**
   * @param list<\Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem> $items
   */
  private function __construct(
    private readonly string $langcode,
    private readonly array $items,
  ) {}

  public static function fromStorage(TranslationItemStorageInterface $storage): self {
    return new self($storage->getLangcode(), array_values($storage->getTranslationItems()));
  }

  public function getLangcode(): string {
    return $this->langcode;
  }

  public function readTranslationItem(): ?TranslationItem {
    return $this->items[$this->currentItemKey++] ?? NULL;
  }

}

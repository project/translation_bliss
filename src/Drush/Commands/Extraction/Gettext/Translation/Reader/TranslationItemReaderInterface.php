<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Reader;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem;

/**
 * @internal
 */
interface TranslationItemReaderInterface {

  public function getLangcode(): string;

  public function readTranslationItem(): ?TranslationItem;

}

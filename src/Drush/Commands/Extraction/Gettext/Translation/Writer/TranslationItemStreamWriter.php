<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Writer;

use Drupal\Component\Gettext\PoHeader;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Reader\TranslationItemReaderInterface;

/**
 * @internal
 */
final class TranslationItemStreamWriter implements TranslationItemWriterInterface {

  /**
   * @var resource
   */
  private readonly mixed $fd;

  public function __construct(
    string $uri,
    PoHeader $header,
    array $generatedFromFiles = [],
  ) {
    /** @see \Drupal\Component\Gettext\PoStreamWriter::open */
    $this->fd = fopen($uri, 'w');
    $this->write($header);
    if ($generatedFromFiles) {
      $this->write(implode("\n", array_merge([
        '# Generated from:',
      ], array_map(
        fn(string $fileName) => "# - $fileName",
        $generatedFromFiles,
      ), ['', '', '',])));
    }
  }

  public function __destruct() {
    fclose($this->fd);
  }

  private function write(string|\Stringable $data): void {
    $data = strval($data);
    $result = fwrite($this->fd, $data);
    if ($result === FALSE || $result != strlen($data)) {
      throw new \Exception('Unable to write data: ' . substr($data, 0, 20));
    }
  }

  public function writeFromReader(TranslationItemReaderInterface $reader): void {
    while ($item = $reader->readTranslationItem()) {
      $this->write($item->toString());
    }
  }

}

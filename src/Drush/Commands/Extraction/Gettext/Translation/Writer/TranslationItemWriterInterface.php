<?php

namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Writer;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Reader\TranslationItemReaderInterface;

/**
 * @internal
 */
interface TranslationItemWriterInterface {

  public function writeFromReader(TranslationItemReaderInterface $reader): void;

}

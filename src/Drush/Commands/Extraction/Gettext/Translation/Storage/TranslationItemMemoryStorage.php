<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Storage;

use Drupal\Component\Gettext\PoStreamReader;
use Drupal\locale\StringStorageInterface;
use Drupal\locale\TranslationString;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage\SourceItemMemoryStorage;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Storage\SourceItemStorageInterface;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem;

/**
 * @internal
 */
final class TranslationItemMemoryStorage implements TranslationItemStorageInterface {

  /**
   * @var array<string, TranslationItem>
   */
  private readonly array $translationItems;

  private function __construct(
    private readonly string $langcode,
    TranslationItem ...$translationItems,
  ) {
    $indexedTranslationItems = [];
    foreach ($translationItems as $translationItem) {
      // The following is not true whe adding untranslated strings.
      // assert($this->langcode === $translationItem->langcode);
      $indexedTranslationItems[$translationItem->getStorageKey()] = $translationItem;
    }
    ksort($indexedTranslationItems, SORT_NATURAL | SORT_FLAG_CASE);
    $this->translationItems = $indexedTranslationItems;
  }

  public static function empty(string $langcode = ''): self {
    return new self($langcode);
  }

  public static function fromCoreStringStorage(string $langcode, StringStorageInterface $stringStorage): self {
    $items = [];
    foreach ($stringStorage->getTranslations(['language' => $langcode]) as $translationString) {
      assert($translationString instanceof TranslationString);
      $items[] = TranslationItem::fromCoreTranslationString($langcode, $translationString);
    }
    return new self($langcode, ...$items);
  }

  public static function fromSourceCollection(SourceItemStorageInterface $storage) {
    $items = [];
    foreach ($storage->getItems() as $item) {
      $items[] = TranslationItem::fromSource($item, '');
    }
    return new self('', ...$items);
  }

  public static function fromPoFile(string $fileUri, string $langcode = ''): self {
    /** @see \Drupal\Core\StringTranslation\Translator\FileTranslation::filesToArray */
    $coreReader = new PoStreamReader();
    $coreReader->setURI($fileUri);
    $coreReader->setLangcode($langcode);
    $coreReader->open();

    $items = [];
    while ($coreItem = $coreReader->readItem()) {
      $items[] = TranslationItem::fromCoreItem($coreItem);
    }
    return new self($langcode, ...$items);
  }

  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * @return list<TranslationItem>
   */
  public function getTranslationItems(): array {
    return array_values($this->translationItems);
  }

  public function getSameSourceTranslationItem(SourceItem $sourceItem): ?TranslationItem {
    return $this->translationItems[$sourceItem->getStorageKey()] ?? NULL;
  }

  public function filterHavingSourceStrings(SourceItemStorageInterface $sources): self {
    $translations = [];
    foreach ($this->getTranslationItems() as $translation) {
      if ($sources->getSameSourceItem(SourceItem::fromTranslation($translation))) {
        $translations[] = $translation;
      }
    }
    return new self($this->langcode, ...$translations);
  }

  public function filterNotHavingSourceStrings(SourceItemStorageInterface $sources): self {
    $translations = [];
    foreach ($this->getTranslationItems() as $translation) {
      if (!$sources->getSameSourceItem(SourceItem::fromTranslation($translation))) {
        $translations[] = $translation;
      }
    }
    return new self($this->langcode, ...$translations);
  }


  public function withFallback(TranslationItemStorageInterface $translationItemCollection): self {
    // Add ours last, so they overwrite the other.
    $translations = array_merge($translationItemCollection->getTranslationItems(), $this->getTranslationItems());
    return new self($this->langcode, ...$translations);
  }

  public function onlyEmpty(): self {
    $translations = [];
    foreach ($this->getTranslationItems() as $item) {
      if ($item->hasEmptyTranslation()) {
        $translations[] = $item;
      }
    }
    return new self($this->langcode, ...$translations);
  }

  public function onlyCustomized(): self {
    $translations = [];
    foreach ($this->getTranslationItems() as $item) {
      if (!$item->hasEmptyTranslation() && $item->customized === TRUE) {
        $translations[] = $item;
      }
    }
    return new self($this->langcode, ...$translations);
  }

  public function onlyNotCustomized(): self {
    $translations = [];
    foreach ($this->getTranslationItems() as $item) {
      if (!$item->hasEmptyTranslation() && $item->customized === FALSE) {
        $translations[] = $item;
      }
    }
    return new self($this->langcode, ...$translations);
  }

  public function sources(): SourceItemMemoryStorage {
    $sources = [];
    foreach ($this->getTranslationItems() as $translation) {
      $sources[] = SourceItem::fromTranslation($translation);
    }
    return new SourceItemMemoryStorage(...$sources);
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Storage;

use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Source\Item\SourceItem;
use Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem;

/**
 * @internal
 */
interface TranslationItemStorageInterface {

  public function getLangcode(): string;

  /**
   * @return list<\Drupal\translation_bliss\Drush\Commands\Extraction\Gettext\Translation\Item\TranslationItem>
   */
  public function getTranslationItems(): array;

  public function getSameSourceTranslationItem(SourceItem $sourceItem): ?TranslationItem;

}

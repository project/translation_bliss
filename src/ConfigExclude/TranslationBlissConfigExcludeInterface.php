<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\ConfigExclude;

use Drupal\Core\TypedData\TypedDataInterface;

/**
 * @api Custom modules can decorate this service.
 */
interface TranslationBlissConfigExcludeInterface {

  /**
   * Exclude config from being translated.
   *
   * Originally needed because webform module marks its yaml translatable, but
   * suppresses translating **any** string that is valid Yaml in the translation
   * form.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $element
   *   The typed data item to inspect.
   *
   * @return bool
   * @see webform_form_locale_translate_edit_form_alter
   *
   */
  public function isConfigTranslationExcluded(TypedDataInterface $element): bool;

}

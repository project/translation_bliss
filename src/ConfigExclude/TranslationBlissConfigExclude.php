<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\ConfigExclude;

use Drupal\Core\TypedData\TypedDataInterface;

/**
 * @internal
 */
final class TranslationBlissConfigExclude implements TranslationBlissConfigExcludeInterface {

  /**
   * Here we suppress translating webform.webform.*:elements.
   */
  public function isConfigTranslationExcluded(TypedDataInterface $element): bool {
    if (str_starts_with($element->getPropertyPath(), 'webform.webform.')) {
      $configName = $element->getRoot()->getPropertyPath();
      return $element->getPropertyPath() === "$configName.elements";
    }
    return FALSE;
  }

}

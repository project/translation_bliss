<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\SourceLanguage;

use Drupal\Core\Language\LanguageInterface;

/**
 * @api
 */
final class SrcLang {

  private function __construct(
    private readonly string $langcode,
  ) {}

  public static function create(string $langcode): self {
    return new self($langcode);
  }

  public function context(string $context = ''): string {
    if ($this->langcode === 'en' || $this->langcode === LanguageInterface::LANGCODE_SYSTEM) {
      return $context;
    }
    return ContextWithMetadata::parse($context)
      ->withKeyValue('srclang', $this->langcode)
      ->toString();
  }

  public function tOptions(array $options): array {
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $options = [
      'context' => $this->context($options['context'] ?? ''),
    ] + $options;
    return $options;
  }

}

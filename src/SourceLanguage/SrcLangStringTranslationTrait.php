<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\SourceLanguage;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

trait SrcLangStringTranslationTrait {

  use StringTranslationTrait;

  protected function tFrom(string $srcLang, string $string, array $args = [], array $options = []): TranslatableMarkup {
    return $this->t($string, $args, SrcLang::create($srcLang)->tOptions($options));
  }

  protected function formatPluralFrom(string $srcLang, int $count, string $singular, string $plural, array $args = [], array $options = []): PluralTranslatableMarkup {
    return $this->formatPlural($count, $singular, $plural, $args, SrcLang::create($srcLang)->tOptions($options));
  }

}

<?php

declare(strict_types=1);
namespace Drupal\translation_bliss\SourceLanguage;

/**
 * Handles translation string context metadata.
 *
 * Dumps and parses [prop=val] prefixes.
 *
 * @internal
 */
final class ContextWithMetadata {

  public readonly array $metadata;

  protected string $asString;

  public function __construct(
    public readonly string $context,
    array                  $metadata,
  ) {
    ksort($metadata);
    $this->metadata = $metadata;
  }

  public static function parse(string $context) {
    $metadata = [];
    while (preg_match('/\[([^]])+?=([^]])*].*$/um', $context, $matches)) {
      [, $key, $value, $context] = $matches;
      $metadata[$key] = $value;
    }
    return new static($context, $metadata);
  }

  protected function serialize() {
    $prefixList = array_map(
      fn(string $value, string|int $key) => "[$key=$value]",
      $this->metadata,
      array_keys($this->metadata)
    );
    $prefix = implode('', $prefixList);
    return "{$prefix}{$this->context}";
  }

  public function toString() {
    return $this->asString ??
      ($this->asString = $this->serialize());
  }

  public function withKeyValue(string $key, ?string $value): static {
    $metadata = $this->metadata;
    if (isset($value)) {
      $metadata[$key] = $value;
    }
    else {
        unset($metadata[$key]);
    }
    return new static($this->context, $metadata);
  }

}

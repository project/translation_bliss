# Translation Bliss TODO
- Drop shipped translation files for non-current versions after drush runs
- Extend translation status to cover shared strings (or do sth better)
- Prefix config contexts by config name / prefix:
  - Import and export untranslatable flag
  - Create config prefix / context mapping in config
  - Listen to ConfigEvents::SAVE and update contexts (copy, as we don't know)

## Upstream
- (Core/TranslationServer) Find a way to identify untranslated and shipped-but-custom-translated strings
- (Drush) Hook implementation is not picked up from trait

# TranslationBliss

Opinionated **total translation control** for ambitious site builders, developers, and site owners.

## The big picture
### Total translation export / import with `drush tex` and `drush tim`
**All** module and custom translatable strings and their translations in one directory **under git control**.
### Simplified and unified config translations
**All** config profits from UI-translations, **and** allows contextual overrides.
### Translation of non-english source strings
Want to convert a monolingual non-english site to multilingual?
Simply **translate your non-english strings** to any language, thanks to source language translation contexts.
### Total config language control
Get a **langcode selector** on every config form.
Get drush commands to diagnose and **bulk convert config langcodes**.

## How to use: Total translation export / import with `drush tex` and `drush tim`
- Install the base module (API and Drush commands) with `drush en translation_bliss`
- For total control of contrib translations, run `drush bliss:translations-dir-init` or `drush tdi`,
  which configures the core translation directory (containing shipped translations for core and contrib)
  to be `../translations`.
  You can change that directory or even omit that step, but this is the well-tested configuration.
  This way, **all translation changes** take the way over a git commit.
- To export custom and config translations, run `drush bliss:translations-extract` or `drush tex`, which
  - extracts all translatable strings from `modules/custom/*` code (via potx module)
  - extracts all config translatable strings
  - tracks all locations where these strings come from
  - exports all translations and separates custom / contrib translatable strings, and customized / shipped translations
  - writes all that info into a structured set of files
- You can then (see file list below)
  - find all custom and config translatable strings and translations in `../translations/custom`
  - add and update translations
  - mark strings as untranslatable
- To re-import your translations, run `drush bliss:translations-import` or `drush tim`
  (make it part of your deployment workflow!).
- For developers: Translation extraction has an extension API. See it in action in `TranslationExtractionHooksBase` and its inheritors.

## Exported files
For a english / german site with "translate to english" enabled, `drush tex` command exports these files:
- custom-strings.all-with-origins.pot
- custom-strings.translatable.pot
- custom-strings.en.po
- custom-strings.de.po
- custom-translated-contrib-strings.en.po
- custom-translated-contrib-strings.de.po
- contrib-translated-common-strings.en._po
- contrib-translated-common-strings.de._po
- adjustments/custom-strings.untranslatable.pot

Let's inspect them.

### custom-strings.all-with-origins.pot
All translatable strings and where they have been found. It contains entries like so:
```
#: File:modules/custom/c4c/src/Plugin/Block/C4cCheckoutProgressBlock.php:19
#: Config:block.block.c4ccheckoutprogress:settings.label
msgid "CrowdPower Checkout"
msgstr ""
```

### custom-strings.translatable.pot
All translatable strings without translations (pot = template). It contains entries like so:
```
msgid "CrowdPower Checkout"
msgstr ""
```

### custom-strings.XX.po
All custom translations of custom strings. It contains entries like so:
```
msgid "CrowdPower Checkout"
msgstr "CrowdPower Vertragsformular"
```

### custom-translated-contrib-strings.XX.po
If you change a translation from a contrib module, it gets an invisible "customized" mark and populates this file.
It contains entries like so:
```
msgid "Annymous user"
msgstr "Wer zum Geier?"
```

Note: Additionally, "vanished" translatable strings (deleted from code or config) end up here, because the module
does not find them (anymore) in custom strings and "thinks" they must be contrib strings.
(Due to core limitations, a fix needs scanning all module translation files. Patch appreciated.)

### contrib-translated-common-strings.XX._po
Does your custom code or config use a "common" translatable string like "Depth", i.e. one that is already used
in core or contrib and translated there (and does not have a customized translation)? Here they are, just for your info, enlightenment, and to have "total translation control".
(The "_po" extension means "do not import". The translations contained in this file are already contained in module
or core po files and imported from there.)

It contains entries like so:
```
msgid "Depth"
msgstr "Tiefe"
```

Note: Due to translation server limitations (no untranslated strings provided), this does not common strings with no shipped translation. Help with an upstream solution appreciated.

### adjustments/custom-strings.untranslatable.pot
- As "adjustments" signals, this file is filed by **you**.
- Any string added here is excluded from translations export and does not clutter up the other translation files.
- This is useful for
  - config strings that are not really translatable,
  - or for admin-facing custom strings that you never want to translate.
- It contains entries like so:

```
msgid "%"
msgstr ""

msgid ", "
msgstr ""

msgid "Hello admin, my old friend!"
msgstr ""
```

## Core limitation: Untranslated contrib strings
The *total translation control* that the above yields, has one blind spot:
If a contrib module has a translatable string with no translation, TranslationBliss can not know.

This is because the Drupal community translation server does not provide info about such untranslated strings
(and extracting core and all modules ourselves would take ages).
A translation server update feature request may pave the way to improve this.
Help appreciated.


## How to use: Simplified and unified config translations
- Enable with `drush en translation_bliss_config`
- Usage **instead of** core config_translation module
  - Translates every translatable config strings with the corresponding interface translation. As simple as that.
- Usage **in addition to** core config_translation module
  - For legacy sites: Existing language overrides for core and contrib are not needed anymore, you can and should delete them (todo: Write drush command for this, patch appreciated)
  - All config can now be translated via interface translations (this was not possible before for custom config)
  - If an interface translation does not fit for a specific config, that translation **can** be overridden via a config translation override (this was not possible before for shipped core or contrib config: UI and shipped-config translations were synced back and forth to always be identical).
  - In the end, usually most or all translations are done via interface translations, and only a few config translation overrides are needed, which **can** be effectively reviewed (before, it was not practically possible to tell the "real" overrides from shipped UI translation copies).
  - If a config is "featured" to config/sync, nothing changes (before, it did a lot, by makeing it "shipped config" forcing translations to be identical to UI translations).

## How to use: Translation of non-english source strings
- Needs the base module (API and Drush commands), install with `drush en translation_bliss`
- For developers: See usage in `SrcLangTest` and `SrcLangStringTranslationTrait`.
- Non-english translatable config strings get a "srclang" context prefix like so:
```
msgctxt "[srclang=de]"
msgid "Schnitzel"
msgstr ""
```

## How to use: Total config language control
- Enable with `drush en translation_bliss_config_language_select`
- Every config form now has a language selector (which changes config langcode, but does not do any auto-translation).
- Also, in the base module there are drush commands to audit and change config language in bulk.
